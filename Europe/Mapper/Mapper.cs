﻿using Europe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Europe.Mapper
{
	public static class Mapper
	{
		public static User ToUserModel(this DAL.Data.User user)
		{
            return (user == null) ? null : new User()
            {
                Id = user.Id,
                Date = user.Date,
                Name = user.Name,
                Email = user.Email,
                Pwd = user.Pwd,
                Active = user.Active,
                Deleted = user.Deleted,
                RoleType = user.RoleType,
                Token = user.Token,
			};
		}

		public static StudentCourse ToStudentCourseModel(this DAL.Data.StudentCourse studentCourse)
		{
			return (studentCourse == null) ? null : new StudentCourse()
			{
				BeginDate = studentCourse.BeginDate,
				CourseId = studentCourse.CourseId,
				CourseImgId = studentCourse.CourseImgId,
				CourseTitle = studentCourse.CourseTitle,
				Points = studentCourse.Points,
                StudentId = studentCourse.StudentId,
                Bookmark = studentCourse.Bookmark,
			};
		}

        public static DAL.Data.StudentCourse ToStudentCourseDal(this StudentCourse studentCourse)
        {
            return (studentCourse == null) ? null : new DAL.Data.StudentCourse()
            {
                BeginDate = studentCourse.BeginDate,
                CourseId = studentCourse.CourseId,
                CourseImgId = studentCourse.CourseImgId,
                CourseTitle = studentCourse.CourseTitle,
                Points = studentCourse.Points,
                StudentId = studentCourse.StudentId,
                Bookmark = studentCourse.Bookmark,
            };
        }

		public static Student ToStudentModel(this DAL.Data.Student student)
		{
			return (student == null) ? null : new Student()
			{
				Id = student.Id,
				UserId = student.UserId,
				ImgId = student.ImgId,
			};
		}

		public static DAL.Data.Student ToDAL(this Student student)
		{
			return (student == null) ? null : new DAL.Data.Student()
			{
				Id = student.Id,
				UserId = student.UserId,
				ImgId = student.ImgId,
			};
		}

		public static DAL.Data.Course ToDAL(this Course course)
		{
			return (course == null) ? null : new DAL.Data.Course()
			{
				//Id = course.Id,
				Title = course.Title,
				Description = course.Description,
				Level = course.Level.ToString(),
				Active = course.Active,
				Deleted = course.Deleted,
				CreationDate = course.CreationDate,
				UpdateDate = course.UpdateDate,
				PublicationDate = course.PublicationDate,
				IdCreator = course.IdCreator,
				IdPublisher = course.IdPublisher,
				CertificateId = course.CertificateId,
				ImgId = course.ImgId,
			};
		}

		public static Course ToCourseModel(this DAL.Data.Course course)
		{
			return (course == null) ? null : new Course()
			{
				Id = course.Id,
				Title = course.Title,
				Description = course.Description,
				Level = course.Level,
				Active = course.Active,
				Deleted = course.Deleted,
				CreationDate = course.CreationDate,
				UpdateDate = course.UpdateDate,
				PublicationDate = course.PublicationDate,
				IdCreator = course.IdCreator,
				IdPublisher = course.IdPublisher,
				CertificateId = course.CertificateId,
				ImgId = course.ImgId,
			};
		}

		public static Chapter ToChapterModel(this DAL.Data.Chapter chapter)
		{
			return (chapter == null) ? null : new Chapter()
			{
				Id = chapter.Id,
				Num = chapter.Num,
				Title = chapter.Title,
				Active = chapter.Active,
				Deleted = chapter.Deleted,
				CourseId = chapter.CourseId,
				ImgId = chapter.ImgId,
			};
		}

		public static DAL.Data.Chapter ToChapterDal(this Chapter chapter)
		{
			return (chapter == null) ? null : new DAL.Data.Chapter()
			{
				Id = chapter.Id,
				Num = chapter.Num,
				Title = chapter.Title,
				Active = chapter.Active,
				Deleted = chapter.Deleted,
				CourseId = chapter.CourseId,
				ImgId = chapter.ImgId,
			};
		}

		public static Certificate ToCertificateModel(this DAL.Data.Certificate certificate)
		{
			return (certificate == null) ? null : new Certificate()
			{
				Id = certificate.Id,
				Title = certificate.Title,
				Description = certificate.Description,
				Difficulty = certificate.Difficulty,
				Active = certificate.Active,
				ImgId = certificate.ImgId,
			};
		}

		public static DAL.Data.Certificate ToCertificateDal(this Certificate certificate)
		{
			return (certificate == null) ? null : new DAL.Data.Certificate()
			{
				Id = certificate.Id,
				Title = certificate.Title,
				Description = certificate.Description,
				Difficulty = certificate.Difficulty,
				Active = certificate.Active,
				ImgId = certificate.ImgId,
			};
		}

		public static Lesson ToLessonModel(this DAL.Data.Lesson lesson)
		{
			return (lesson == null) ? null : new Lesson()
			{
				Id = lesson.Id,
				Title = lesson.Title,
				Num = lesson.Num,
				Active = lesson.Active,
				Deleted = lesson.Deleted,
				ChapterId = lesson.ChapterId,
				ImgId = lesson.ImgId,
			};
		}

		public static DAL.Data.Lesson ToLessonDal(this Lesson lesson)
		{
			return (lesson == null) ? null : new DAL.Data.Lesson()
			{
				Id = lesson.Id,
				Title = lesson.Title,
				Num = lesson.Num,
				Active = lesson.Active,
				Deleted = lesson.Deleted,
				ChapterId = lesson.ChapterId,
				ImgId = lesson.ImgId,
			};
		}

		public static Page ToPageModel(this DAL.Data.Page page)
		{
			return (page == null) ? null : new Page()
			{
				Id = page.Id,
				Num = page.Num,
				Title = page.Title,
				Content = page.Content,
				IsQuizz = page.IsQuizz,
				Active = page.Active,
				Deleted = page.Deleted,
				ChapterId = page.ChapterId,
				LessonId = page.LessonId,
			};
		}

		public static DAL.Data.Page ToPageDal(this Page page)
		{
			return (page == null) ? null : new DAL.Data.Page()
			{
				Id = page.Id,
				Num = page.Num,
				Title = page.Title,
				Content = page.Content,
				IsQuizz = page.IsQuizz,
				Active = page.Active,
				Deleted = page.Deleted,
				ChapterId = page.ChapterId,
				LessonId = page.LessonId,
			};
		}

		public static DAL.Data.QuizzField ToQuizzFieldDal(this QuizzField quizzField)
		{
			return (quizzField == null) ? null : new DAL.Data.QuizzField()
			{
				Active = quizzField.Active,
				Deleted = quizzField.Deleted,
				IsAnswer = quizzField.IsAnswer,
				Id = quizzField.Id,
				PageId = quizzField.PageId,
			};
		}

		public static QuizzField ToQuizzFieldModel(this DAL.Data.QuizzField quizzField)
		{
			return (quizzField == null) ? null : new QuizzField()
			{
				Active = quizzField.Active,
				Deleted = quizzField.Deleted,
				IsAnswer = quizzField.IsAnswer,
				Id = quizzField.Id,
				PageId = quizzField.PageId,
			};
		}

		public static DAL.Data.Badge ToBadgeDal(this Badge badge)
		{
			return (badge == null) ? null : new DAL.Data.Badge()
			{
				Active = badge.Active,
				Deleted = badge.Deleted,
				Description = badge.Description,
				Id = badge.Id,
				ImgId = badge.ImgId,
				Instruction = badge.Instruction,
				Title = badge.Title,
			};
		}

		public static Badge ToBadgeModel(this DAL.Data.Badge badge)
		{
			return (badge == null) ? null : new Badge()
			{
				Active = badge.Active,
				Deleted = badge.Deleted,
				Description = badge.Description,
				Id = badge.Id,
				ImgId = badge.ImgId,
				Instruction = badge.Instruction,
				Title = badge.Title,
			};
		}

		public static StudentBadge ToStudentBadgeModel(this DAL.Data.StudentBadge studentBadge)
		{
			return (studentBadge == null) ? null : new StudentBadge()
			{
				BadgeId = studentBadge.BadgeId,
				Date = studentBadge.Date,
				StudentId = studentBadge.StudentId,
			};
		}

		public static DAL.Data.StudentBadge ToStudentBadgeDal(this StudentBadge studentBadge)
		{
			return (studentBadge == null) ? null : new DAL.Data.StudentBadge()
			{
				BadgeId = studentBadge.BadgeId,
				Date = studentBadge.Date,
				StudentId = studentBadge.StudentId,
			};
		}
		
		//public static Role ToModel(this DAL.Data.Role role)
		//{
		//	return (role == null) ? null : new Role()
		//	{
		//		Id = role.Id,
		//		Type = role.Type,
		//	};
		//}

		public static Img ToModelImg(this DAL.Data.Img img)
		{
			return (img == null) ? null : new Img()
			{
				Id = img.Id,
				Name = img.Name,
				Title = img.Title,
				Data = img.Data,
				Alt = img.Alt,
				Caption = img.Caption,
				Format = img.Format,
				Size = img.Size,
				IsAdmin = img.IsAdmin,
			};
		}

		//public static DAL.Data.Img ToImgDAL(this Img img)
		//{
		//	return (img == null) ? null : new DAL.Data.Img()
		//	{
		//		Data = img.File,
		//		Name = img.Name,
		//		Title = img.Title,
		//		Caption = img.Caption,
		//		Alt = img.Alt,
		//		//Path = img.Path
		//	};
		//}

		//public static ImgDetails ToImgDetailsModel(this DAL.Data.Img img)
		//{
		//	return (img == null) ? null : new ImgDetails()
		//	{
		//		File = img.Data,
		//		Format = img.Format,
		//		Name = img.Name,
		//		Title = img.Title,
		//		Caption = img.Caption,
		//		Alt = img.Alt,
		//	};
		//}







		// Conversion objets POCO --> Objets Models
		//public static Models.App ToASP(this DAL.Data.App App)
		//{
		//    return (App == null) ? null : new Models.App()
		//    {
		//        AppId = App.App_Id,
		//        App_Title = App.App_Title,
		//        App_Baseline = App.App_Baseline,
		//        App_FB_URL = App.App_FB_URL,
		//        App_Twitter_URL = App.App_Twitter_URL,
		//        App_Credits = App.App_Credits,
		//    };
		//}

		// Conversion objets POCO --> Objets Models
		//public static Models.Course ToASP(this DAL.Data.Course Course)
		//{
		//    return (Course == null) ? null : new Models.Course()
		//    {
		//        Id = Course.Id,
		//        Title = Course.Title,
		//        Description = Course.Description,
		//        Active = Course.Active,
		//        Delete = Course.Deleted,
		//        CreationDate = Course.CreationDate,
		//        UpdateDate = Course.UpdateDate,
		//        PublicationDate = Course.PublicationDate,
		//        IdCreator = Course.IdCreator,
		//        IdPublisher = Course.IdPublisher,
		//        CreatorName = Course.CreatorName,
		//        PublisherName = Course.PublisherName,
		//        Img = Course.Img,
		//    };
		//}

		//public static DAL.Data.Student ToDAL(this Models.ViewModels.Student.StudentForm StudentForm)
		//{
		//    return (StudentForm == null) ? null : new DAL.Data.Student()
		//    {
		//        Name = StudentForm.Name,
		//        Email = StudentForm.Email,
		//        Pwd = StudentForm.Pwd,
		//        RoleType = StudentForm.RoleType,
		//    };
		//}

		//public static Models.HomeCourse ToHomeCourse(this DAL.Data.Course Course)
		//{
		//	return (Course == null) ? null : new Models.HomeCourse()
		//	{
		//		Id = Course.Id,
		//		Title = Course.Title,
		//		Description = Course.Description,
		//	};
		//}

		//public static Models.Img ToASP(this DAL.Data.Img Image)
		//{
		//	return (Image == null) ? null : new Models.Img()
		//	{
		//		Id = Image.Id,
		//		Name = Image.Name,
		//		Title = Image.Title,
		//		File = Image.Data,
		//		Alt = Image.Alt,
		//		Caption = Image.Caption,
		//		Path = Image.Path,
		//		Size = Image.Size
		//	};
		//}

		//public static DAL.Data.Img ToImgDAL(this Models.Img Image)
		//{
		//	return (Image == null) ? null : new DAL.Data.Img()
		//	{
		//		Id = Image.Id,
		//		Name = Image.Name,
		//		Title = Image.Title,
		//		Data = Image.File,
		//		Alt = Image.Alt,
		//		Caption = Image.Caption,
		//		Path = Image.Path,
		//		Size = Image.Size
		//	};
		//}
	}
}