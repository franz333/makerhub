﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Europe.Models
{
	public class Certificate
	{
		public int Id { get; set; }

        [Display(Name = "Titre")]
		[Required(ErrorMessage ="N'oublie pas le titre du certificat")]
        public string Title { get; set; }

		[Display(Name = "Description")]
		[DataType(DataType.MultilineText)]
		[Required(ErrorMessage = "N'oublie pas de décrire le certificat")]
		public string Description { get; set; }

		[Display(Name = "Niveau")]
        public string Difficulty { get; set; }

        [Display(Name = "Actif")]
        public bool Active { get; set; }

        [Display(Name = "Logo")]
        public int ImgId { get; set; }

		[Display(Name = "Image")]
		[DataType(DataType.Upload)]
		public HttpPostedFileBase File { get; set; }

		public int CourseId { get; set; }
	}
}