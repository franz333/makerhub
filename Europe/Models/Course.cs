﻿using DAL.Services;
using Europe.Custom_ExtensionMethods;
using Europe.Mapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Europe.Models
{
	public class Course
	{
		[Display(Name = "Id")]
		[HiddenInput(DisplayValue = false)]
		public int Id { get; set; }

		[Display(Name = "Titre")]
		public string Title { get; set; }

		[Display(Name = "Description")]
		[DataType(DataType.MultilineText)]
		public string Description { get; set; }
		
		public List<string> Levels = new List<string>() { "facile", "intermédiare", "difficile"};

		[Display(Name = "Niveau")]
		public string Level { get; set; }

		[Display(Name = "Actif")]
		public bool Active { get; set; }

		public string IsPublished
		{
			get { return Active.ToPublishString(); }
		}

		[Display(Name = "Supprimé")]
		public bool Deleted { get; set; }

		public string IsDeleted
		{
			get { return Deleted.ToYesNoString(); }
		}

		private DateTime _CreationDate;
        [Display(Name = "Création")]
        [DisplayFormat(DataFormatString = "{0:d-M-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CreationDate
		{
			get
			{
				if (_CreationDate == null)
				{
					_CreationDate = DateTime.Now;
					return _CreationDate;
				}
				else
				{
					return _CreationDate;
				}				
			}
			set { _CreationDate = value; }
		}

		[Display(Name = "Mise à jour")]
		[DisplayFormat(DataFormatString = "{0:d-M-yyyy}", ApplyFormatInEditMode = true)]
		public DateTime? UpdateDate { get; set; }

		[Display(Name = "Publication")]
		[DisplayFormat(DataFormatString = "{0:d-M-yyyy}", ApplyFormatInEditMode = true)]
		public DateTime? PublicationDate { get; set; }

		[Display(Name = "Id Créateur")]
		private int _IdCreator;
		public int IdCreator
		{
			get
			{
				if (_IdCreator == 0)
				{
					_IdCreator = 1; // Par défault le créateur du cours est l'Admin principal
					return _IdCreator;
				}
				else
				{
					return _IdCreator;
				}				
			}
			set { _IdCreator = value; }
		}

		[Display(Name = "Id Editeur")]
		public int? IdPublisher { get; set; }

		[Display(Name = "Auteur")]
		public string CreatorName { get; set; }

		[Display(Name = "Editeur")]
		public string PublisherName { get; set; }

		[Display(Name = "Image")]
		[DataType(DataType.Upload)]
		public HttpPostedFileBase File { get; set; }
		
		[Display(Name = "Logo")]
        public int ImgId { get; set; }
        

		[Display(Name = "Id du certificat")]
		public int CertificateId { get; set; }

		private Certificate _Certificate;
		public Certificate Certificate
		{
			get
			{
				if (_Certificate == null)
				{
					CertificateService certificateService = new CertificateService();
					_Certificate = certificateService.Get(CertificateId).ToCertificateModel();
				}
				return _Certificate; 
			}
		}

		[Display(Name = "Chapitres")]
		private IEnumerable<Chapter> _Chapters;
		public IEnumerable<Chapter> Chapters
		{
			get
			{
				if (_Chapters == null)
				{
					ChapterService chapterService = new ChapterService();
					_Chapters = chapterService.GetChaptersByCourse(Id).Select(c => c.ToChapterModel()).OrderBy(o => o.Num).ToList();

					// ---- Définir la classe de l'item pour l'affichage ---- //					

					string classItem = "col-12";
					int count = _Chapters.Count();

					// Boucle sur la collection de Lessons
					for (int i = 0; i < count; i++)
					{
						// Le premier et le dernier élément sont sur disposés une colonne qui occupe toute la largeur
						if ((i == 0) || (i == count - 1))
						{
							_Chapters.ElementAt(i).CssClass = classItem;
						}
						// L'avant dernier élément varie en fonctionne du nombre de lesson (pair ou impair)
						else if (i == count - 2)
						{
							if (count % 2 == 0)
							{
								_Chapters.ElementAt(i).CssClass = "col-6";
							}
							else
							{
								_Chapters.ElementAt(i).CssClass = classItem;
							}
						}
						else
						{
							_Chapters.ElementAt(i).CssClass = "col-6";
						}
					}
				}
				return _Chapters;
			}
		}

		
	}
}
