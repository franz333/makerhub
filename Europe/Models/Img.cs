﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Europe.Models
{
	public class Img
	{
		public int Id { get; set; }
		public byte[] Data { get; set; }
		public string Format { get; set; }
		public string Name { get; set; }

		private int _Size;
		public int Size
		{
			get
			{
				return _Size / 1024;
			}
			set
			{
				_Size = value;
			}
		}
		public string Title { get; set; }
		public string Alt { get; set; }
		public string Caption { get; set; }
		public bool IsAdmin { get; set; }
	}
}