﻿using DAL.Services;
using Europe.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Europe.Models
{
	public class Asso
	{
		public int Id { get; set; }

		public int UserId { get; set; }

		public int ImgId { get; set; }

		private Img _Img;
		public Img Img
		{
			get
			{
				if (_Img == null)
				{
					ImgService imgService = new ImgService();
					_Img = imgService.Get(ImgId).ToModelImg();					
				}
				return _Img;
			}
		}

		private DAL.Data.User _User;
		public DAL.Data.User User
		{
			get
			{
				if (_User == null)
				{
					UserService userService = new UserService();
					_User = userService.Get(UserId);
				}
				return _User;
			}
		}

		public string Street { get; set; }

		public string Number { get; set; }

		public string Box { get; set; }

		public string City { get; set; }

		public string Country { get; set; }

		public string Website { get; set; }

		public string Description { get; set; }

		public int PicNumber { get; set; }

		public string Sector { get; set; }

	}
}