﻿using DAL.Services;
using Europe.Mapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Europe.Models
{
	public class UserLogin
	{
		[HiddenInput(DisplayValue = false)]
		public int RoleType { get; set; }

		[HiddenInput(DisplayValue = false)]
		public int UserId { get; set; }

		[Required]
		[Display(Name = "Identifiant")]
		[DataType(DataType.EmailAddress)]
        [Remote("IsEmailRegistered", "Account", HttpMethod = "POST", ErrorMessage = "Cette adresse e-mail ne correspond à aucun compte")]        
        public string Email { get; set; }

		[Required]
		[Display(Name = "Mot de passe")]
		//[StringLength(16, MinimumLength = 2)]
		[DataType(DataType.Password)]
		public string Pwd { get; set; }

		public bool LoginCheck()
		{
			UserService service = new UserService();
			User user = service.GetByEmail(Email).ToUserModel();
			if (user != null && (user.Email == Email && user.Pwd == Pwd))
			{
				UserId = user.Id;
				RoleType = user.RoleType;
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}