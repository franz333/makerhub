﻿using DAL.Services;
using Europe.Mapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Europe.Models
{
	public class StudentUpdateForm
	{
		[HiddenInput(DisplayValue = false)]
		public int Id { get; set; }

		[HiddenInput(DisplayValue = false)]
		public int UserId { get; set; }

		[HiddenInput(DisplayValue = false)]
		public int ImgId { get; set; }

		[Display(Name = "Image")]
		[DataType(DataType.Upload)]
		public HttpPostedFileBase File { get; set; }

		[Required(ErrorMessage = "N'oublie pas ton pseudo")]
		public string Name { get; set; }

		[Required(ErrorMessage = "N'oublie pas ton adresse e-mail")]
		public string Email { get; set; }

		public bool IsFlagSet { get; set; }

		[DataType(DataType.Password)]
		[Required(ErrorMessage = "Renseigne ton mot de passe actuel")]
		//[System.ComponentModel.DataAnnotations.Compare(nameof(Pwd), ErrorMessage = "Le mot de passe ne correspond pas")]
		//[System.ComponentModel.DataAnnotations.Compare("Pwd", ErrorMessage = "Le mot de passe ne correspond pas.")]
		public string CurrentPwd { get; set; }

		[DataType(DataType.Password)]
		[Required(ErrorMessage = "Renseigne ton nouveau mot de passe")]
		public string NewPwd { get; set; }

		[DataType(DataType.Password)]
		[Required(ErrorMessage = "Renseigne la confirmation du nouveau mot de passe")]
		//[System.ComponentModel.DataAnnotations.Compare("NewPwd", ErrorMessage = "Le mot de passe est vide ou ne correspond pas")]
		[System.ComponentModel.DataAnnotations.Compare(nameof(NewPwd), ErrorMessage = "Le mot de passe est vide ou ne correspond pas")]
		public string ConfirmPwd { get; set; }

		public StudentUpdateForm(int id)
		{
			Student student = new Student();
			StudentService studentService = new StudentService();
			student = studentService.Get(id).ToStudentModel();
			Id = student.Id;
			UserId = student.UserId;
			ImgId = student.ImgId;
			Name = student.User.Name;
			Email = student.User.Email;
			CurrentPwd = "********";//student.User.Pwd;
		}

		public StudentUpdateForm()
		{
		}
	}
}