﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Europe.Models
{
	public class StudentForm
	{

		[HiddenInput(DisplayValue = false)]
		public int RoleType
		{
			get	{ return 2; }
		}

		[HiddenInput(DisplayValue = false)]
		public int ImgId { get; set; }

		[Required(ErrorMessage = "N'oublie pas ton pseudo")]
		[Display(Name = "Nom")]
		public string Name { get; set; }

		[DataType(DataType.EmailAddress)]
        [Remote("IsEmailFree", "Account", HttpMethod = "POST", ErrorMessage = "L'adresse e-mail est déjà existante")]
        [Required(ErrorMessage = "N'oublie pas ton adresse e-mail")]
		[Display(Name = "E-mail")]
		public string Email { get; set; }

		[DataType(DataType.EmailAddress)]
		[Required(ErrorMessage = "Confirme ton adresse e-mail")]
		[Display(Name = "Confirmation de l'E-mail")]
		[System.ComponentModel.DataAnnotations.Compare(nameof(Email), ErrorMessage = "La confirmation de l'e-mail est vide ou ne correspond pas")]
		public string ConfirmEmail { get; set; }

		[DataType(DataType.Password)]
		[Required(ErrorMessage = "Renseigne ton mot de passe")]
		[Display(Name = "Mot de passe")]
		public string Pwd { get; set; }

		[DataType(DataType.Password)]
		[Required(ErrorMessage = "Confirme ton mot de passe")]
		[Display(Name = "Confirmation du mot de passe")]
		[System.ComponentModel.DataAnnotations.Compare(nameof(Pwd), ErrorMessage = "Le mot de passe est vide ou ne correspond pas")]
		public string ConfirmPwd { get; set; }
	}
}