﻿using DAL.Services;
using Europe.Mapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Europe.Models
{
	public class StudentBadge
	{
		[DisplayFormat(DataFormatString = "{0:d-M-yyyy}", ApplyFormatInEditMode = true)]
		public DateTime Date { get; set; }

		public int StudentId { get; set; }
		public int BadgeId { get; set; }

		private Badge _Badge;
		public Badge Badge
		{
			get
			{
				if (_Badge == null)
				{
					BadgeService badgeService = new BadgeService();
					_Badge = badgeService.Get(BadgeId).ToBadgeModel();
				}
				return _Badge;
			}
		}


	}
}