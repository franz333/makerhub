﻿using DAL.Data;
using DAL.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Europe.Models
{
	public class User
	{
		//[Display(Name = "ID")]
		public int Id { get; set; }

		//[Display(Name = "Nom")]
		[Required]
		[Display(Name = "Mot de passe", Description = "N'oublie pas ton pseudo !")]
		public string Name { get; set; }

		//[Display(Name = "Date de création")]
		public DateTime Date { get; set; }

		//[Display(Name = "Email")]
		[Required]
		[Display(Name = "E-mail", Description = "N'oublie pas ton e-mail !")]
		public string Email { get; set; }

		[Required]
		[Display(Name = "Mot de passe", Description = "Votre mot de passe doit contenir au minimum 8 caractères et être composé d'une majuscule et d'un chiffre.")]
		[RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,12}$")]
		[DataType(DataType.Password)]
		public string Pwd { get; set; }

		//[Display(Name = "Actif")]
		public bool Active { get; set; }

		//[Display(Name = "Supprimé")]
		public bool Deleted { get; set; }

		//[Display(Name = "Rôle")]
		public int RoleType { get; set; }

        public Guid? Token { get; set; }

        public int StudentId { get; set; }
        public int AssoId { get; set; }

        // TABLE ROLE
        // ------------------------------------------

        //private Role _Role;
        //public Role Role
        //{
        //	get
        //	{
        //		if (_Role == null)
        //		{
        //			RoleService roleService = new RoleService();
        //			_Role = roleService.Get(RoleType).ToModel();
        //		}
        //		return _Role;
        //	}
        //}
    }
}