﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Europe.Models
{
	public class ChangePwd
	{
		[DataType(DataType.Password)]
		
		//[System.ComponentModel.DataAnnotations.Compare(nameof(Pwd), ErrorMessage = "Le mot de passe ne correspond pas")]
		public string CurrentPwd { get; set; }

		[DataType(DataType.Password)]
		public string NewPwd { get; set; }

		[DataType(DataType.Password)]
		[Compare(nameof(NewPwd), ErrorMessage = "Le mot de passe ne correspond pas")]
		public string ConfirmPwd { get; set; }
	}
}