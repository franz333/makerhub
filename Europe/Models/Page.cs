﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAL.Services;
using Europe.Mapper;

namespace Europe.Models
{
	public class Page
	{
		public int Id { get; set; }
		public int Num { get; set; }
		public string Title { get; set; }
		public string Content { get; set; }
		public bool IsQuizz { get; set; }
		public bool Active { get; set; }
		public bool Deleted { get; set; }
		public int ChapterId { get; set; }
		public int LessonId { get; set; }
		// Utile pour le calcul véritable des points
		public bool Visited { get; set; }


		private IEnumerable<QuizzField> _QuizzFields;
		public IEnumerable<QuizzField> QuizzFields
		{
			get
			{
				if (_QuizzFields == null)
				{
					QuizzFieldService quizzFieldService = new QuizzFieldService();
					_QuizzFields = quizzFieldService.GetByPageId(Id).Select(c => c.ToQuizzFieldModel()).OrderBy(o => o.Id).ToList();
				}
				return _QuizzFields;
			}
		}
	}
}