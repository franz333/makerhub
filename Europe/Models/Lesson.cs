﻿using DAL.Services;
using Europe.Mapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Europe.Models
{
	public class Lesson
	{
		//[HiddenInput(DisplayValue = false)]
		public int Id { get; set; }

		[Required(ErrorMessage = "La lesson nécessite un numéro")]
		[Range(typeof(int), "0", "10")]
		public int Num { get; set; }

		[Required(ErrorMessage = "La lesson nécessite un titre")]
		public string Title { get; set; }

		[HiddenInput(DisplayValue = false)]
		public bool Active { get; set; }

		[HiddenInput(DisplayValue = false)]
		public bool Deleted { get; set; }

		private int _ImgId;
		public int ImgId
		{
			get
			{
				_ImgId = 6;
				return _ImgId;
			}
			set { _ImgId = value; }
		}


		[Display(Name = "Pages")]
		private IEnumerable<Page> _Pages;
		public IEnumerable<Page> Pages
		{
			get
			{
				if (_Pages == null)
				{
					PageService pageService = new PageService();
					_Pages = pageService.GetByLessonId(Id).Select(c => c.ToPageModel()).OrderBy(n =>n.Num).ToList();
				}
				return _Pages;
			}
		}
        
        public int CourseId { get; set; }
        public int ChapterId { get; set; }

	}
}