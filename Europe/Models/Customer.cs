﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Europe.Models
{
	public class Customer
	{
		///<summary>
		/// Gets or sets Name.
		///</summary>
		public string Name { get; set; }

		///<summary>
		/// Gets or sets DateTime.
		///</summary>
		public string DateTime { get; set; }

		public int Id { get; set;}
	}
}