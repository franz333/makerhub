﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Europe.Models
{
	public class Image
	{
		public int Id { get; set; }
		public int UserId { get; set; }
		public HttpPostedFileBase ImageFile { get; set; }
		public string Name { get; set; }
		public string Title { get; set; }
		public string Alt { get; set; }
	}
}