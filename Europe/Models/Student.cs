﻿using DAL.Services;
using Europe.Mapper;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace Europe.Models
{
	public class Student
	{
		// TABLE STUDENT
		// ------------------------------------------

		[HiddenInput(DisplayValue = false)]
		public int Id { get; set; }

		[HiddenInput(DisplayValue = false)]
		public int UserId { get; set; }

		public int ImgId { get; set; }

        public int RoleTpe
        {
            get { return 2; }
        }

		// TABLE USER
		// ------------------------------------------

		private DAL.Data.User _User;
		public DAL.Data.User User
		{
			get
			{
				if (_User == null)
				{
					UserService userService = new UserService();
					_User = userService.Get(UserId);
				}
				return _User;
			}
		}

		// TABLE STUDENTCOURSE
		// ------------------------------------------

		private IEnumerable<StudentCourse> _StudentCourses;
		public IEnumerable<StudentCourse> StudentCourses
		{
			get
			{
				if (_StudentCourses == null)
				{
					CourseService courseService = new CourseService();
					_StudentCourses = courseService.GetCoursesByStudentId(Id).Select(c => c.ToStudentCourseModel()).ToList();
				}
				return _StudentCourses;
			}
		}

		public int Points; 

		public int GetPointsByCourse(int courseId)
		{
			// On récupère le studentCourse d'un étudiant
			CourseService serv = new CourseService();
			StudentCourse studentCourse = serv.GetByStudentIdAndCourseId(Id, courseId).ToStudentCourseModel();
						
			Points = studentCourse.Points;
			return Points;
		}

		private int _TotaPoints;
		public int TotalPoints
		{			
			get
			{
				if (_StudentCourses != null)
				{
					List<StudentCourse> list = _StudentCourses.ToList();

					for (int i = 0; i < list.Count(); i++)
					{
						_TotaPoints += list[i].Points;
					}
					return _TotaPoints;
				}
				else
				{
					_TotaPoints = -1;
					return _TotaPoints;
				}				
			}
		}

		// TABLE CERTIFICATE
		// ------------------------------------------

		[Display(Name = "Certificats obtenus")]
		public IEnumerable<Certificate> Certificates { get; set; }

		// TABLE STUDENTBADGE
		// ------------------------------------------

		[Display(Name = "Badges obtenus")]
		private IEnumerable<StudentBadge> _StudentBadges;
		public IEnumerable<StudentBadge> StudentBadges
		{
			get
			{
				if (_StudentBadges == null)
				{
					StudentBadgeService studentBadgeService = new StudentBadgeService();
					_StudentBadges = studentBadgeService.GetByStudentId(Id).Select(c => c.ToStudentBadgeModel()).ToList();					
				}
				return _StudentBadges;
			}
		}

		public string CssClass { get; set; }
	}
}