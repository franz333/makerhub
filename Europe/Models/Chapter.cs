﻿using DAL.Services;
using Europe.Mapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Europe.Models
{
	public class Chapter
	{
		//[HiddenInput(DisplayValue = false)]
		public int Id { get; set; }

		[Required(ErrorMessage = "Le chapitre nécessite un numéro")]
		[Range(typeof(int), "0", "10")]
		public int Num { get; set; }

		[Required(ErrorMessage = "Le chapitre nécessite un titre")]
		public string Title { get; set; }

		[HiddenInput(DisplayValue = false)]
		public bool Active { get; set; }

		[HiddenInput(DisplayValue = false)]
		public bool Deleted { get; set; }

		public int CourseId { get; set; }

		private int _ImgId;
		[Display(Name = "Logo")]
		public int ImgId
		{
			get
			{

				if (_ImgId != 0)
				{
					return _ImgId;
				}
				else
				{
					return 5; //default value
				}
			}
			set { _ImgId = value; }
		}

		[Display(Name = "Image")]
		[DataType(DataType.Upload)]
		public HttpPostedFileBase File { get; set; }

		public string CssClass { get; set; }

		[Display(Name = "Lessons")]
		private IEnumerable<Lesson> _Lessons;
		public IEnumerable<Lesson> Lessons
		{
			get
			{
				if (_Lessons == null)
				{
					LessonService lessonService = new LessonService();
					_Lessons = lessonService.GetByChapter(Id).Select(c => c.ToLessonModel()).OrderBy(o => o.Num).ToList();					
				}
				return _Lessons;
			}
			set { _Lessons = value; }
		}
	}
}