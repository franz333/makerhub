﻿using DAL.Services;
using Europe.Mapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Europe.Models
{
	public class StudentCourse
	{
        public DateTime BeginDate { get; set; }
		public int Points { get; set; }
		public int CourseId { get; set; }
		public string CourseTitle { get; set; }
		public int CourseImgId { get; set; }
        public int StudentId { get; set; }
        public string Bookmark { get; set; }

		private Student _Student;
		public Student Student
		{
			get
			{
				if (_Student == null)
				{
					StudentService studentService = new StudentService();
					_Student = studentService.Get(StudentId).ToStudentModel();
				}
				return _Student;
			}
		}

		private Course _Course;
		public Course Course
		{
			get
			{
				if (_Course == null)
				{
					CourseService courseService = new CourseService();
					_Course = courseService.Get(CourseId).ToCourseModel();
				}
				return _Course;
			}
		}

	}
}