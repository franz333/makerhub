﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Europe.Models
{
	public class ImgUploadForm
	{
		public int Id { get; set; }

		[Required]
		[Display(Name = "Nom")]
		public string Name { get; set; }

		[DataType(DataType.Upload)]
		public HttpPostedFileBase File { get; set; }

		[Required]
		[Display(Name = "Titre")]
		public string Title { get; set; }

		[Required]
		[Display(Name = "Texte alternatif")]
		public string Alt { get; set; }

		[Display(Name = "Caption")]
		public string Caption { get; set; }

		public bool IsAdmin { get; set; }

		[Display(Name = "Taille")]
		public int Size { get; set; }
	}
}