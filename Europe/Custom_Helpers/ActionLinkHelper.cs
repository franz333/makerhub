﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Routing;

namespace Europe.Custom_Helpers
{
     public static class ActionLinkHelper
	 {
		// Lien Ajax Action Link wrappé dans une image avec titre en dessous en option
		public static IHtmlString ImageAjaxActionLink(
            this AjaxHelper helper, 
            string imageUrl, 
            string actionName, 
            object routeValues, 
            AjaxOptions ajaxOptions, 
            object htmlAttributes, 
            string title = null)
        {
            var builder = new TagBuilder("img");
            builder.MergeAttribute("src", imageUrl);
            builder.MergeAttributes(new RouteValueDictionary(htmlAttributes));
			var link = helper.ActionLink("[replaceme]", actionName, routeValues, ajaxOptions).ToHtmlString();

			if (title != null)
			{
				var builderText = new TagBuilder("span");
				builderText.InnerHtml = title;
				return MvcHtmlString.Create(link.Replace("[replaceme]", builder.ToString(TagRenderMode.SelfClosing) 
                    + builderText.ToString(TagRenderMode.Normal)));
			}
			else
			{
				return MvcHtmlString.Create(link.Replace("[replaceme]", builder.ToString(TagRenderMode.SelfClosing)));
			}			
        }

		// Lien Ajax Action Link wrappé dans une balise titre
		public static IHtmlString HeadingAjaxActionLink(this AjaxHelper helper, string urlText, string actionName, object routeValues, AjaxOptions ajaxOptions, object htmlAttributes, int heading)
		{
			var builder = new TagBuilder(string.Format("h{0}", heading));
			builder.InnerHtml = urlText;
			builder.MergeAttributes(new RouteValueDictionary(htmlAttributes));
			var link = helper.ActionLink("[replaceme]", actionName, routeValues, ajaxOptions).ToHtmlString();
			return MvcHtmlString.Create(link.Replace("[replaceme]", builder.ToString(TagRenderMode.Normal)));
		}

		// Lien Action Link wrappé dans une balise <a> contenant une balise <img> et ses attributs avec titre en dessous en option
		public static IHtmlString ImageActionLink(this HtmlHelper htmlHelper, string imageUrl, string actionName, string controller, object routeValues, object htmlAttributes, string alt = null, string imgTitle = null, string title = null)
		{
			// Création du tag <img> avec différents attributs
			var builderImg = new TagBuilder("img");
			builderImg.MergeAttribute("src", imageUrl);

			if (alt != null)
			{
				builderImg.MergeAttribute("alt", alt);
			}

			if (imgTitle != null)
			{
				builderImg.MergeAttribute("title", imgTitle);
			}

			builderImg.MergeAttributes(new RouteValueDictionary(htmlAttributes));

			// Création du tag <a> avec le href
			var builderAnchor = new TagBuilder("a");
			builderAnchor.InnerHtml = builderImg.ToString(TagRenderMode.SelfClosing);

			// Création du tag <span> avec le texte
			if (title != null)
			{
				var builderText = new TagBuilder("span");
				builderText.InnerHtml = title;
				builderAnchor.InnerHtml = builderImg.ToString(TagRenderMode.SelfClosing) + builderText.ToString(TagRenderMode.Normal);
			}
					   
			var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);
			builderAnchor.Attributes["href"] = urlHelper.Action(actionName, controller, routeValues);
			builderAnchor.MergeAttributes(new RouteValueDictionary());

			return MvcHtmlString.Create(builderAnchor.ToString());
		}

        // Trim de texte à 150 caractères
        public static IHtmlString Post(this HtmlHelper helper, string postContent)
        {
            string postStr = postContent;
            if (postStr.Length > 147)
            {
                postStr = postStr.Substring(0, 147) + "...";
            }
            return MvcHtmlString.Create(postStr);
        }
    }
}