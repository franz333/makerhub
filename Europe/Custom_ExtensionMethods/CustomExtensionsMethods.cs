﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Europe.Custom_ExtensionMethods
{
	public static class CustomExtensionsMethods
	{
		public static string ToPublishString(this bool b)
		{
			return b ? "Publié" : "A publier";
		}

		public static string ToYesNoString(this bool b)
		{
			return b ? "Oui" : "Non";
		}
	}
}