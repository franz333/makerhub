﻿using DAL.Services;
using Europe.Mapper;
using Europe.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Europe.Controllers
{
    public class ImgController : Controller
    {
		// GET : Img by ID
		public ActionResult Get(int id)
		{
			ImgService imgService = new ImgService();
			Img img = new Img();
			img = imgService.Get(id).ToModelImg();
			byte[] imgByteArray = img.Data;
			string formatWithoutPoint = img.Format.Substring(1);
			string arg = "image/" + formatWithoutPoint;
			return File(imgByteArray, arg);
		}

		// Récupération de toutes les images : gestion admin des images
		public ActionResult GetAll()
		{
			ImgService imgService = new ImgService();
			List<Img> imgs = imgService.Get().Select(c => c.ToModelImg()).ToList();
			return PartialView("_ImgAll", imgs);
		}

		// Affichage de l'avatar via vue partielle
		public ActionResult DisplayImg(int imgId)
		{
			ImgService imgService = new ImgService();
			Img img = new Img();
			img = imgService.Get(imgId).ToModelImg();
			return PartialView("_Img", img);
		}
		
		// Affichage de l'image via Json : gestion admin des images
		public JsonResult GetImgJson(int imgId)
		{
			ImgService imgService = new ImgService();
			Img img = new Img();
			img = imgService.Get(imgId).ToModelImg();
			return Json(img, JsonRequestBehavior.AllowGet);
		}

		// Suppression de l'avatar dans l'édition du profil utilisateur
		public ActionResult DeleteAvatar(int imgId, int userId)
		{
			// Suppression de l'image de l'utilisateur -- côté DB un set default suit le delete
			ImgService imgService = new ImgService();
			imgService.Delete(imgId);

			// Récupération du type d'utilisateur : l'image par défaut correspond au RoleType
			UserService s = new UserService();
			int roleType = s.Get(userId).RoleType;

			//// Renvoyer l'image par défaut à la vue partielle
			Img img = new Img();
			img = imgService.Get(roleType).ToModelImg();
			//return PartialView("_TestImg", img);
			return Json(img, JsonRequestBehavior.AllowGet);
		}
	}
}
