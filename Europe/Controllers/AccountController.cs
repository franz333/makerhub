﻿using DAL.Services;
using Europe.Mapper;
using Europe.Models;
using Europe.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Europe.Controllers
{
    public class AccountController : Controller
    {
		[AnonymousRequired]
		public ActionResult Login()
		{
			UserLogin login = new UserLogin();
			return View(login);
		}

		[HttpPost]
		[AnonymousRequired]
		public ActionResult Login(UserLogin login, string returnUrl)
		{
			if (ModelState.IsValid)
			{
				// Si le user est authentifié
				if (login.LoginCheck())
				{
					// Création d'une session
					UserSession.User = new User()
					{
						Id = login.UserId,
						RoleType = login.RoleType,
					};

					// S'il est un étudiant on le redirige vers son compte utilisateur Student
					if (login.RoleType == 2)
					{
						StudentService service = new StudentService();
						Student student = new Student();
						student = service.GetByUserId(login.UserId).ToStudentModel();
						UserSession.User.StudentId = student.Id;
						if (!string.IsNullOrWhiteSpace(returnUrl) && Url.IsLocalUrl(returnUrl))
							return Redirect(returnUrl);
						return RedirectToAction("Get", "Student", new { id = student.Id });

					}
					else
					{
						// Code pour récupérer le compte d'une association
						return RedirectToAction("Get", "Association");
					}
				}

				// Si le user n'est pas authentifié
				else
				{
					// Ajouter un TempData pour afficher un lien vers une page de création de compte Association
					ModelState.AddModelError("Utilisateur.Prenom", "Prénom et/ou mot de passe incorrect(s)");
					return RedirectToAction("Account", "Student");
				}
			}
			return View(login);
		}

		public ActionResult Disconnect()
		{
			Session.Clear();
			Session.Abandon();
			return RedirectToAction("Index", "Home");
		}

		[AllowAnonymous]
		public ActionResult Add()
		{
			StudentForm studentForm = new StudentForm();
			studentForm.ImgId = 2;
			return View(studentForm);
		}

		[HttpPost]
		[AllowAnonymous]
		public ActionResult Add(StudentForm studentForm, string returnUrl)
		{
			// Insertion d'un User et récupération de l'Id dans un Student
			Student student = new Student();
			UserService userService = new UserService();
			student.UserId = userService.Insert(new DAL.Data.User()
			{
				Name = studentForm.Name,
				Email = studentForm.Email,
				Pwd = studentForm.Pwd,
				Date = DateTime.Now,
				RoleType = studentForm.RoleType,
			});

			// Insertion d'un Student
			StudentService studentService = new StudentService();
			student.Id = studentService.Insert(new DAL.Data.Student()
			{
				UserId = student.UserId,
				ImgId = studentForm.ImgId,
			});

			// Création d'une session
			UserSession.User = new User()
			{
				Id = student.UserId,
				RoleType = student.RoleTpe,
				StudentId = student.Id,
			};

			if (!string.IsNullOrWhiteSpace(returnUrl) && Url.IsLocalUrl(returnUrl))
				return Redirect(returnUrl);
			return RedirectToAction("Get", "Student", new { id = student.Id });
		}

		// Remote validation for email on client side : does it exist? || is it free?
		[HttpPost]
		public JsonResult IsEmailFree(string Email)
		{
			return Json(IsEmailAvailable(Email));
		}

		[HttpPost]
		public JsonResult IsEmailRegistered(string Email)
		{
			return Json(!IsEmailAvailable(Email));
		}

		public bool IsEmailAvailable(string email)
		{
			UserService userService = new UserService();
			bool status;
			if (userService.GetByEmail(email) == null)
			{
				status = true;
			}
			else
			{
				status = false;
			}
			return status;
		}

		// TODO 
		[AllowAnonymous]
		public ActionResult AddAsso()
		{
			StudentForm studentForm = new StudentForm();
			studentForm.ImgId = 2;
			return View(studentForm);
		}
	}
}