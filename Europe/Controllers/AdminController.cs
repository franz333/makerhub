﻿using DAL.Services;
using Europe.Mapper;
using Europe.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Europe.Controllers
{
	public class AdminController : Controller
	{
		// GET: Admin
		public ActionResult Index()
		{
			return View();
		}

		// IMG AREA ------------------------------------------------------------------------------
		#region Img

		[HttpGet] // DONE
		public ActionResult InsertorUpdateImg()
		{
			ImgUploadForm imgUploadForm = new ImgUploadForm();
			imgUploadForm.IsAdmin = false;
			return View(imgUploadForm);
		}

		[HttpPost] // DONE
		public ActionResult InsertorUpdateImg(ImgUploadForm imgUploadForm)
		{
			if (!ModelState.IsValid)
			{
				var errors = ModelState
				.Where(x => x.Value.Errors.Count > 0)
				.Select(x => new { x.Key, x.Value.Errors })
				.ToArray();
				return View(imgUploadForm);
			}
			else
			{
				// Si une image est présente on la récupère
				if (imgUploadForm.File != null && imgUploadForm.File.ContentType.Contains("image"))
				{
					string[] formats = new string[] { ".jpg", ".png", ".gif", ".jpeg" };

					if (formats.Any(item => imgUploadForm.File.FileName.EndsWith(item, StringComparison.OrdinalIgnoreCase)))
					{
						// Méta données de l'image
						string imgFormat = Path.GetExtension(imgUploadForm.File.FileName);
						int imgSize = imgUploadForm.File.ContentLength;

						// Encodage de l'image en byte array
						byte[] data;
						using (Stream inputStream = imgUploadForm.File.InputStream)
						{
							MemoryStream memoryStream = inputStream as MemoryStream;
							if (memoryStream == null)
							{
								memoryStream = new MemoryStream();
								inputStream.CopyTo(memoryStream);
							}
							data = memoryStream.ToArray();
						}

						// Insertion d'une nouvelle image
						// De base la valeur IsAdmin est false donc il s'agit d'une nouvelle image
						if (imgUploadForm.IsAdmin == false)
						{
							ImgService serv = new ImgService();
							imgUploadForm.Id = serv.Insert(new DAL.Data.Img()
							{
								Data = data,
								Format = imgFormat,
								Name = imgUploadForm.Name,
								Size = imgSize,
								Alt = imgUploadForm.Alt,
								Caption = imgUploadForm.Caption,
								IsAdmin = true,
							});							
							ViewBag.SuccessUpload = "Upload de l'image réussie";
							ImgUploadForm newImgUploadForm = new ImgUploadForm();
							return View(newImgUploadForm);
						}
						else
						{
							// Update de l'image existante
							// Quand la valeur IsAdmin est true
							ImgService serv = new ImgService();
							serv.Update(new DAL.Data.Img()
							{
								Id = imgUploadForm.Id,
								Data = data,
								Format = imgFormat,
								Name = imgUploadForm.Name,
								Size = imgSize,
								Alt = imgUploadForm.Alt,
								Caption = imgUploadForm.Caption,
								IsAdmin = true,
							});
							ViewBag.SuccessUpload = "Update de l'image réussie";
							ImgUploadForm newImgUploadForm = new ImgUploadForm();
							return View(newImgUploadForm);
						}
					}
					else
					{
						// avertir l'admin que le format n'est pas correct
						// TODO : tester si l'instanciation d'un nouveau form est utile, je ne crois pas
						ViewBag.SuccessUpload = "Le format n'est pas correct";						
						return View(imgUploadForm);
					}
				}
				else
				{
					if (imgUploadForm.Id == 0)
					{
						ViewBag.SuccessUpload = "Il n'y a aucun fichier uploadé";
						return View(imgUploadForm);
					}
					else
					{
						// S'il n'y a pas de fichier, modifier les champs attributs de l'image qui ont peut-être subi des modifications
						ImgService serv = new ImgService();
						serv.UpdateImgFields(new DAL.Data.Img()
						{
							Id = imgUploadForm.Id,
							Title = imgUploadForm.Title,
							Name = imgUploadForm.Name,
							Alt = imgUploadForm.Alt,
							Caption = imgUploadForm.Caption,
						});
						ViewBag.SuccessUpload = "Update de l'image réussie";
						ImgUploadForm newImgUploadForm = new ImgUploadForm();
						return View(newImgUploadForm);
					}
				}
			}
		}

		//public ActionResult DeleteImg(int imgId)
		//{
		//          ImgUploadForm newImgUploadForm = new ImgUploadForm();
		//	return View(newImgUploadForm);
		//}

		#endregion


		// COURSES AREA ------------------------------------------------------------------------------

		[HttpGet] // DONE
		public ActionResult Courses()
		{
			List<Course> courses = new List<Course>();
			CourseService courseService = new CourseService();
			courses = courseService.Get().Select(c => c.ToCourseModel()).ToList();
			return View(courses);
		}

		[HttpGet] // DONE
		public ActionResult CourseDetails(int id)
		{
			Course course = new Course();
			CourseService courseService = new CourseService();
			course = courseService.Get(id).ToCourseModel();
			if (TempData["AddCourseMessage"] != null)
			{
				ViewBag.AddCourse = TempData["AddCourseMessage"].ToString();
			}
			return View(course);
		}

		[HttpGet] // DONE
		public ActionResult AddCourse()
		{
			ViewBag.Level = new List<string>() { "facile", "intermédiaire", "difficile" };
			Course course = new Course();
            course.ImgId = 4;
			return View(course);
		}

		[HttpPost] // DONE
		public ActionResult AddCourse(Course course)
		{

			if (!ModelState.IsValid)
			{
				return View(course);
				//var errors = ModelState
				//.Where(x => x.Value.Errors.Count > 0)
				//.Select(x => new { x.Key, x.Value.Errors })
				//.ToArray();
			}
			else
			{
				// Si une image est présente on la récupère
				if (course.File != null && course.File.ContentType.Contains("image"))
				{
					string[] formats = new string[] { ".jpg", ".png", ".gif", ".jpeg" };

					// Si le format est bon, on poursuit l'enregistrement de l'image
					if (formats.Any(item => course.File.FileName.EndsWith(item, StringComparison.OrdinalIgnoreCase)))
					{
						// Méta données de l'image 
						string imgFormat = Path.GetExtension(course.File.FileName);
						int imgSize = course.File.ContentLength;

						string nametreated = course.Title.Replace(" ", "-").ToLower();
						string imgName = "avatar-cours-" + nametreated;

						string imgTitle = "Avatar du cours : " + course.Title;
						string imgAlt = "Image représentant le cours : " + course.Title + " de Europe&Un";

						// Encodage de l'image en byte array
						byte[] data;
						using (Stream inputStream = course.File.InputStream)
						{
							MemoryStream memoryStream = inputStream as MemoryStream;
							if (memoryStream == null)
							{
								memoryStream = new MemoryStream();
								inputStream.CopyTo(memoryStream);
							}
							data = memoryStream.ToArray();
						}

						// Insertion de l'image
						ImgService serv = new ImgService();
						course.ImgId = serv.Insert(new DAL.Data.Img()
						{
							Data = data,
							Format = imgFormat,
							Size = imgSize,
							Name = imgName,
							Title = imgTitle,
							Alt = imgAlt,
							IsAdmin = true,
						});

					}
					else
					{
						// avertir l'admin qu'il ne s'agit pas d'un fichier image
						ViewBag.FileError = "Le fichier n'est pas une fichier image";
						return View(course);
					}
				}
				else
				{
					// S'il n'y a pas d'image ajoutée, on s'assure que le cours conserve l'image par défaut
					course.ImgId = 4;
				}

				// On insère le nouveau cours
				CourseService courseService = new CourseService();

				// Création d'un certificat car la DB en demande un
				Certificate certificate = new Certificate();
				certificate.Title = course.Title;
				certificate.Difficulty = course.Level;
				certificate.Description = "Certificat de suivi du cours : " + course.Title;
				certificate.Active = false;

				CertificateService certificateService = new CertificateService();
				certificate.Id = certificateService.Insert(certificate.ToCertificateDal());

				// Enregistrement des données du cours
				course.Id = courseService.Insert(new DAL.Data.Course()
				{
					Title = course.Title,
					Description = course.Description,
					CreationDate = DateTime.Now,
					ImgId = course.ImgId,
					Level = course.Level,
					IdCreator = course.IdCreator,
					CertificateId = certificate.Id,
				});
				// Lorsqu'on utilise RedirectToAction, on passe un message avec TempData qu'on récupère dans le controller vers lequel on redirige
				TempData["AddCourseMessage"] = "Le cours a été bien ajouté";
				//ViewBag.AddCourse = "Le cours a été bien ajouté";
				return RedirectToAction("CourseDetails", "Admin", new { course.Id });
			}
		}		

		[HttpGet] // DONE
		public ActionResult EditCourse(int id)
		{
			Course course = new Course();
			CourseService courseService = new CourseService();
			course = courseService.Get(id).ToCourseModel();
			return View(course);
		}

		[HttpPost] // DONE
		public ActionResult EditCourse(Course course)
		{
			// Le créateur du cours est l'admin
			course.IdCreator = 1;

			if (!ModelState.IsValid)
			{
				return View(course);
			}
			else
			{
				// Si une image est présente on la récupère
				if (course.File != null && course.File.ContentType.Contains("image"))
				{
					string[] formats = new string[] { ".jpg", ".png", ".gif", ".jpeg" };

					if (formats.Any(item => course.File.FileName.EndsWith(item, StringComparison.OrdinalIgnoreCase)))
					{
						// Méta données de l'image 
						// TODO : créer une image directement et éviter le mapping ligne 195 par ex
						string imgFormat = Path.GetExtension(course.File.FileName);
						int imgSize = course.File.ContentLength;

						string nametreated = course.Title.Replace(" ", "-").ToLower();
						string imgName = "avatar-cours-" + nametreated;

						string imgTitle = "Avatar du cours : " + course.Title;
						string imgAlt = "Image représentant le cours : " + course.Title + " de Europe&Un";

						// Encodage de l'image en byte array

						byte[] data;
						using (Stream inputStream = course.File.InputStream)
						{
							MemoryStream memoryStream = inputStream as MemoryStream;
							if (memoryStream == null)
							{
								memoryStream = new MemoryStream();
								inputStream.CopyTo(memoryStream);
							}
							data = memoryStream.ToArray();
						}

						ImgService serv = new ImgService();
						if (course.ImgId == 4)
						{
							// Insertion de l'image
							course.ImgId = serv.Insert(new DAL.Data.Img()
							{
								Data = data,
								Format = imgFormat,
								Size = imgSize,
								Name = imgName,
								Title = imgTitle,
								Alt = imgAlt,
								IsAdmin = true,
							});
						}
						else
						{
							serv.Update(new DAL.Data.Img()
							{
								Id = course.ImgId,
								Data = data,
								Format = imgFormat,
								Size = imgSize,
								Name = imgName,
								Title = imgTitle,
								Alt = imgAlt,
								IsAdmin = true,
							});
						}
					}
					else
					{
						// avertir l'admin qu'il ne s'agit pas d'un fichier image
						return View(course);
					}
				}
				
				// On update le cours
				CourseService courseService = new CourseService();
				DateTime now = DateTime.Now;
                courseService.Update(new DAL.Data.Course()
                {
                    Id = course.Id,
                    Title = course.Title,
                    Description = course.Description,
                    UpdateDate = now,
                    ImgId = course.ImgId,
                    Level = course.Level,
                    IdCreator = course.IdCreator,
                    Active = true,                    
				});
			}
			return RedirectToAction("CourseDetails", "Admin", new { id = course.Id });
		}

		// TODO
		public ActionResult DeleteCourse(Course course)
		{
			return View();
		}

		// CHAPTERS AREA ------------------------------------------------------------------------------

		[HttpGet] // DONE
		public ActionResult ChapterDetails(int chapterId, int courseId)
		{
			Course course = new Course();
			CourseService courseService = new CourseService();
			course = courseService.Get(courseId).ToCourseModel();
			ViewBag.CourseTitle = course.Title;
			Chapter chapter = new Chapter();
			ChapterService chapterService = new ChapterService();
			if (TempData["AddChapterMessage"] != null)
			{
				ViewBag.AddChapter = TempData["AddChapterMessage"].ToString();
			}
			chapter = chapterService.Get(chapterId).ToChapterModel();
			return View(chapter);
		}

		[HttpGet] // DONE
		public ActionResult AddChapter(int courseId)
		{
			Course course = new Course();
			CourseService courseService = new CourseService();
			course = courseService.Get(courseId).ToCourseModel();
			ViewBag.CourseTitle = course.Title;

			Chapter chapter = new Chapter();
			int num = course.Chapters.Count();

			chapter.CourseId = courseId;
			chapter.Num = num + 1;
			return View(chapter);
		}

		[HttpPost] // DONE
		public ActionResult AddChapter(Chapter chapter)
		{
			if (!ModelState.IsValid)
			{
				return View(chapter);
			}
			else
			{
				// Si une image est présente on la récupère
				if (chapter.File != null && chapter.File.ContentType.Contains("image"))
				{
					string[] formats = new string[] { ".jpg", ".png", ".gif", ".jpeg" };

					if (formats.Any(item => chapter.File.FileName.EndsWith(item, StringComparison.OrdinalIgnoreCase)))
					{
						// Méta données de l'image 
						string imgFormat = Path.GetExtension(chapter.File.FileName);
						int imgSize = chapter.File.ContentLength;

						string nametreated = chapter.Title.Replace(" ", "-").ToLower();
						string imgName = "avatar-chapitre-" + nametreated;

						string imgTitle = "Avatar du chapitre : " + chapter.Title;
						string imgAlt = "Image représentant le chapitre : " + chapter.Title + " d'un cours d'Europe&Un";

						// Encodage de l'image en byte array
						byte[] data;
						using (Stream inputStream = chapter.File.InputStream)
						{
							MemoryStream memoryStream = inputStream as MemoryStream;
							if (memoryStream == null)
							{
								memoryStream = new MemoryStream();
								inputStream.CopyTo(memoryStream);
							}
							data = memoryStream.ToArray();
						}

						ImgService serv = new ImgService();
						// Insertion de l'image
						chapter.ImgId = serv.Insert(new DAL.Data.Img()
						{
							Data = data,
							Format = imgFormat,
							Size = imgSize,
							Name = imgName,
							Title = imgTitle,
							Alt = imgAlt,
							IsAdmin = true,
						});
					}
				}

				ChapterService chapterService = new ChapterService();
				chapter.Id = chapterService.Insert(chapter.ToChapterDal());

				TempData["AddChapterMessage"] = "Le chapitre a été bien ajouté";
				return RedirectToAction("ChapterDetails", "Admin", new { chapterId = chapter.Id, courseId = chapter.CourseId });
			}
		}

		[HttpGet] // DONE
		public ActionResult EditChapter(int chapterId, int courseId)
		{
			Course course = new Course();
			CourseService courseService = new CourseService();
			course = courseService.Get(courseId).ToCourseModel();
			ViewBag.CourseTitle = course.Title;
			Chapter chapter = new Chapter();
			ChapterService chapterService = new ChapterService();
			chapter = chapterService.Get(chapterId).ToChapterModel(); ;
			return View(chapter);
		}

		[HttpPost] // DONE
		public ActionResult EditChapter(Chapter chapter, int courseId)
		{
			if (!ModelState.IsValid)
			{
				return View(chapter);
			}
			else
			{
				// Si une image est présente on la récupère
				if (chapter.File != null && chapter.File.ContentType.Contains("image"))
				{
					string[] formats = new string[] { ".jpg", ".png", ".gif", ".jpeg" };

					if (formats.Any(item => chapter.File.FileName.EndsWith(item, StringComparison.OrdinalIgnoreCase)))
					{
						// Méta données de l'image 
						string imgFormat = Path.GetExtension(chapter.File.FileName);
						int imgSize = chapter.File.ContentLength;

						string nametreated = chapter.Title.Replace(" ", "-").ToLower();
						string imgName = "avatar-chapitre-" + nametreated;

						string imgTitle = "Avatar du chapitre : " + chapter.Title;
						string imgAlt = "Image représentant le cours : " + chapter.Title + " de Europe&Un";

						// Encodage de l'image en byte array
						byte[] data;
						using (Stream inputStream = chapter.File.InputStream)
						{
							MemoryStream memoryStream = inputStream as MemoryStream;
							if (memoryStream == null)
							{
								memoryStream = new MemoryStream();
								inputStream.CopyTo(memoryStream);
							}
							data = memoryStream.ToArray();
						}

						ImgService serv = new ImgService();
						if (chapter.ImgId == 5)
						{
							// Insertion de l'image
							chapter.ImgId = serv.Insert(new DAL.Data.Img()
							{
								Data = data,
								Format = imgFormat,
								Size = imgSize,
								Name = imgName,
								Title = imgTitle,
								Alt = imgAlt,
								IsAdmin = true,
							});
						}
						else
						{
							serv.Update(new DAL.Data.Img()
							{
								Id = chapter.ImgId,
								Data = data,
								Format = imgFormat,
								Size = imgSize,
								Name = imgName,
								Title = imgTitle,
								Alt = imgAlt,
								IsAdmin = true,
							});
						}
					}
					else
					{
						// avertir l'admin qu'il ne s'agit pas d'un fichier image
						return View(chapter);
					}
				}

				// On update le cours
				ChapterService chapterService = new ChapterService();
				chapterService.Update(new DAL.Data.Chapter()
				{
					Id = chapter.Id,
					Title = chapter.Title,
					ImgId = chapter.ImgId,

				});
			}
			return RedirectToAction("CourseDetails", "Admin", new { id = courseId });
		}

		[HttpPost] // DONE
		public EmptyResult SortChapterList(List<string> items)
		{
			// Cast de l'ID	et ajout dans une nouvelle liste de Integer
			List<int> newList = new List<int>();
			int number;

			for (int i = 0; i < items.Count(); i++)
			{				
				items[i] = items[i].Substring(items[i].Length - 1);
				int.TryParse(items[i], out number);
				newList.Add(number);
			}

			ChapterService chapterService = new ChapterService();

			// Update du numéro du chapitre par chapitre Id
			for (int i = 0; i < newList.Count(); i++)
			{
				Chapter chapter = new Chapter();
				chapter.Id = newList[i];
				chapter.Num = i+1;
				chapterService.UpdateChapterOrder(chapter.ToChapterDal());
			}			
			return new EmptyResult();
		}

		// LESSON AREA ------------------------------------------------------------------------------

		[HttpGet] // DONE
		public ActionResult LessonDetails(int lessonId, int courseId)
		{
			LessonService lessonService = new LessonService();
			Lesson lesson = new Lesson();
			lesson = lessonService.Get(lessonId).ToLessonModel();
			lesson.CourseId = courseId;

			Course course = new Course();
			CourseService courseService = new CourseService();
			course = courseService.Get(courseId).ToCourseModel();
			ViewBag.CourseTitle = course.Title;
			ViewBag.CourseId = course.Id;

			Chapter chapter = new Chapter();
			ChapterService chapterService = new ChapterService();
			chapter = chapterService.Get(lesson.ChapterId).ToChapterModel();
			ViewBag.ChapterTitle = chapter.Title;
			ViewBag.ChapterNum = chapter.Num;

			if (TempData["EditLessonMessage"] != null)
			{
				ViewBag.EditLesson = TempData["EditLessonMessage"].ToString();
			}
			return View(lesson);
		}

		[HttpGet] // DONE
		public ActionResult AddLesson(int chapterId, int courseId)
		{
			Course course = new Course();
			CourseService courseService = new CourseService();
			course = courseService.Get(courseId).ToCourseModel();
			ViewBag.CourseTitle = course.Title;
			ViewBag.CourseId = course.Id;

			Chapter chapter = new Chapter();
			ChapterService chapterService = new ChapterService();
			chapter = chapterService.Get(chapterId).ToChapterModel();
			ViewBag.ChapterTitle = chapter.Title;
			ViewBag.ChapterNum = chapter.Num;
			ViewBag.ChapterId = chapter.Id;

			Lesson lesson = new Lesson();
			lesson.Num = chapter.Lessons.Count() + 1;
			return View(lesson);
		}

		[HttpPost] // DONE
		public ActionResult AddLesson(Lesson lesson)
		{
			//TempData["shortMessage"] = "MyMessage";
			LessonService lessonService = new LessonService();
			lesson.Id = lessonService.Insert(lesson.ToLessonDal());
			return RedirectToAction("ChapterDetails", "Admin", new { chapterId = lesson.ChapterId, courseId = lesson.CourseId });
		}

		[HttpGet] // DONE
		public ActionResult EditLesson(int lessonId, int courseId)
		{
			Course course = new Course();
			CourseService courseService = new CourseService();
			course = courseService.Get(courseId).ToCourseModel();
			ViewBag.CourseTitle = course.Title;
			ViewBag.CourseId = course.Id;

			Lesson lesson = new Lesson();
			LessonService lessonService = new LessonService();
			lesson = lessonService.Get(lessonId).ToLessonModel();

			Chapter chapter = new Chapter();
			ChapterService chapterService = new ChapterService();
			chapter = chapterService.Get(lesson.ChapterId).ToChapterModel();
			ViewBag.ChapterNum = chapter.Num;

			return View(lesson);
		}

		[HttpPost] // DONE
		public ActionResult EditLesson(Lesson lesson)
		{
			LessonService lessonService = new LessonService();
			lessonService.Update(lesson.ToLessonDal());

			TempData["EditLessonMessage"] = "La leçon a été bien modifée";
			return RedirectToAction("LessonDetails", "Admin", new { lessonId = lesson.Id, courseId = lesson.CourseId });
		}


		[HttpPost] // DONE
		public EmptyResult SortLessonList(List<string> items)
		{
			// Cast de l'ID	et ajout dans une nouvelle liste de Integer
			List<int> newList = new List<int>();
			int number;

			for (int i = 0; i < items.Count(); i++)
			{
				items[i] = items[i].Substring(items[i].Length - 1);
				int.TryParse(items[i], out number);
				newList.Add(number);
			}

			LessonService lessonService = new LessonService();

			// Update du numéro de la leçon par lessonId
			for (int i = 0; i < newList.Count(); i++)
			{
				Lesson lesson = new Lesson();
				lesson.Id = newList[i];
				lesson.Num = i + 1;
				lessonService.UpdateLessonOrder(lesson.ToLessonDal());
			}
			return new EmptyResult();
		}

		// PAGE AREA ------------------------------------------------------------------------------ 

        [HttpGet]
		public ActionResult AddPage(int lessonId, int chapterId, int courseId)
		{
            CourseService courseService = new CourseService();
            Course course = new Course();
            course = courseService.Get(courseId).ToCourseModel();
            ViewBag.CourseTitle = course.Title;
            ViewBag.CourseId = courseId;

            ViewBag.lessonId = lessonId;
            LessonService lessonService = new LessonService();
            Lesson lesson = new Lesson();
            lesson = lessonService.Get(lessonId).ToLessonModel();
            ViewBag.LessonNum = lesson.Num;

            ChapterService chapterService = new ChapterService();
            Chapter chapter = new Chapter();
            chapter = chapterService.Get(chapterId).ToChapterModel();
            ViewBag.ChapterNum = chapter.Num;

            Page page = new Page();
            page.Num = lesson.Pages.Count()+1;
			page.IsQuizz = false;
            return View(page);
		}

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult AddPage(Page page, int courseId)
        {
            PageService pageService = new PageService();            
            page.Id = pageService.Insert(page.ToPageDal());
            ViewBag.CourseId = courseId;
            return RedirectToAction("PageDetails", "Admin", new { pageId = page.Id, lessonId = page.LessonId, chapterId = page.ChapterId, courseId = courseId });
        }

        public ActionResult PageDetails(int pageId, int lessonId, int chapterId, int courseId)
        {
            CourseService courseService = new CourseService();
            Course course = new Course();
            course = courseService.Get(courseId).ToCourseModel();
            ViewBag.CourseTitle = course.Title;
            ViewBag.CourseId = courseId;

            ChapterService chapterService = new ChapterService();
            Chapter chapter = new Chapter();
            chapter = chapterService.Get(chapterId).ToChapterModel();
            ViewBag.ChapterNum = chapter.Num;

            LessonService serv = new LessonService();
            Lesson lesson = new Lesson();
            lesson = serv.Get(lessonId).ToLessonModel();
            ViewBag.LessonNum = lesson.Num;

            PageService pageService = new PageService();
            Page page = new Page();
            page = pageService.Get(pageId).ToPageModel();
            return View(page);
        }

        [HttpGet]
        public ActionResult EditPage(int pageId, int lessonId, int chapterId, int courseId)
        {
            CourseService courseService = new CourseService();
            Course course = new Course();
            course = courseService.Get(courseId).ToCourseModel();
            ViewBag.CourseTitle = course.Title;
            ViewBag.CourseId = courseId;

            ChapterService chapterService = new ChapterService();
            Chapter chapter = new Chapter();
            chapter = chapterService.Get(chapterId).ToChapterModel();
            ViewBag.ChapterNum = chapter.Num;

            LessonService serv = new LessonService();
            Lesson lesson = new Lesson();
            lesson = serv.Get(lessonId).ToLessonModel();
            ViewBag.LessonNum = lesson.Num;

            PageService pageService = new PageService();
            Page page = new Page();
            page = pageService.Get(pageId).ToPageModel();
            return View(page);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult EditPage(Page page, int lessonId, int chapterId, int courseId)
        {
            PageService pageService = new PageService();
            pageService.Update(page.ToPageDal());
            return RedirectToAction("PageDetails", "Admin", new { pageId = page.Id, lessonId, chapterId, courseId });
        }


        [HttpGet]
		public ActionResult AddQuizzPage(int lessonId, int chapterId, int courseId)
		{
			CourseService courseService = new CourseService();
			Course course = new Course();
			course = courseService.Get(courseId).ToCourseModel();
			ViewBag.CourseTitle = course.Title;
			ViewBag.CourseId = courseId;

			ViewBag.lessonId = lessonId;
			LessonService lessonService = new LessonService();
			Lesson lesson = new Lesson();
			lesson = lessonService.Get(lessonId).ToLessonModel();
			ViewBag.LessonNum = lesson.Num;

			ChapterService chapterService = new ChapterService();
			Chapter chapter = new Chapter();
			chapter = chapterService.Get(chapterId).ToChapterModel();
			ViewBag.ChapterNum = chapter.Num;

			Page page = new Page();
			page.Num = lesson.Pages.Count() + 1;
			page.IsQuizz = true;
			return View(page);
		}

		[HttpPost]
		public ActionResult AddQuizzPage(Page page, int courseId)
		{
			PageService pageService = new PageService();
			page.Id = pageService.Insert(page.ToPageDal());
			ViewBag.CourseId = courseId;
			return RedirectToAction("PageDetails", "Admin", new { pageId = page.Id, lessonId = page.LessonId, chapterId = page.ChapterId, courseId = courseId });
		}

		[HttpGet]
		public ActionResult AddQuizzItem(int pageId)
		{
			QuizzField quizzField = new QuizzField();
			return PartialView("_AddQuizzItem", quizzField);
		}

		[HttpPost]
		public EmptyResult SortPageList(List<string> items)
		{
			// Cast de l'ID	et ajout dans une nouvelle liste de Integer
			List<int> newList = new List<int>();
			int number;

			for (int i = 0; i < items.Count(); i++)
			{
				items[i] = items[i].Substring(items[i].Length - 1);
				int.TryParse(items[i], out number);
				newList.Add(number);
			}

			PageService pageService = new PageService();

			// Update du numéro de la leçon par lessonId
			for (int i = 0; i < newList.Count(); i++)
			{
				Page page = new Page();
				page.Id = newList[i];
				page.Num = i + 1;
				pageService.UpdatePageOrder(page.ToPageDal());
			}
			return new EmptyResult();
		}

        // CERTIFICATE AREA ------------------------------------------------------------------------------

        [HttpGet]
        public ActionResult EditCertificate(int id, int courseId)
        {
            Course course = new Course();
            CourseService courseService = new CourseService();
            course = courseService.Get(courseId).ToCourseModel();
            ViewBag.CourseTitle = course.Title;
            ViewBag.courseId = course.Id;

            CertificateService certificateService = new CertificateService();
            Certificate certificate = new Certificate();
            certificate = certificateService.Get(id).ToCertificateModel();
            return View(certificate);
        }

		[HttpPost]
		public ActionResult EditCertificate(Certificate certificate)
		{
			if (!ModelState.IsValid)
			{
				return View(certificate);
			}
			else
			{
				// Si une image est présente on la récupère
				if (certificate.File != null && certificate.File.ContentType.Contains("image"))
				{
					string[] formats = new string[] { ".jpg", ".png", ".gif", ".jpeg" };

					if (formats.Any(item => certificate.File.FileName.EndsWith(item, StringComparison.OrdinalIgnoreCase)))
					{
						// Méta données de l'image 
						string imgFormat = Path.GetExtension(certificate.File.FileName);
						int imgSize = certificate.File.ContentLength;

						string nametreated = certificate.Title.Replace(" ", "-").ToLower();
						string imgName = "avatar-certificat-" + nametreated;

						string imgTitle = "Avatar du certificat : " + certificate.Title;
						string imgAlt = "Image représentant le certificat : " + certificate.Title + " d'un cours d'Europe&Un";

						// Encodage de l'image en byte array
						byte[] data;
						using (Stream inputStream = certificate.File.InputStream)
						{
							MemoryStream memoryStream = inputStream as MemoryStream;
							if (memoryStream == null)
							{
								memoryStream = new MemoryStream();
								inputStream.CopyTo(memoryStream);
							}
							data = memoryStream.ToArray();
						}

						ImgService serv = new ImgService();
						// Insertion de l'image
						certificate.ImgId = serv.Insert(new DAL.Data.Img()
						{
							Data = data,
							Format = imgFormat,
							Size = imgSize,
							Name = imgName,
							Title = imgTitle,
							Alt = imgAlt,
							IsAdmin = true,
						});
					}
				}
				CertificateService certificateService = new CertificateService();

                certificate.Active = true;
                certificateService.Update(certificate.ToCertificateDal());


                return RedirectToAction("CourseDetails", "Admin", new { id = certificate.CourseId });
			}
		}

    }
}