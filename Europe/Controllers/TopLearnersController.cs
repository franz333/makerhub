﻿using DAL.Services;
using Europe.Mapper;
using Europe.Models;
using Europe.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Europe.Controllers
{
    public class TopLearnersController : Controller
    {
        // GET: TopLearners
        public ActionResult Index()
        {
			StudentService studentService = new StudentService();

			// Récupération de la liste des étudiants
			List<Student> studentList = new List<Student>();
			studentList = studentService.Get().Select(c => c.ToStudentModel()).ToList();

			// Tri des étudiants qui sont inscrit à un ou plusieurs cours
			List<Student> listToDisplay = new List<Student>();

			foreach (var item in studentList)
			{
				if (item.StudentCourses.Count() > 0)
				{
					listToDisplay.Add(item);

					// Si l'étudiant est connecté, son profil ressort du classement
					if (UserSession.User != null && UserSession.User.Id == item.User.Id)
					{
						item.CssClass = "TopLearnerCurrentStudent";
					}
				}
			}

			listToDisplay.OrderBy(t => t.TotalPoints);
			ViewBag.ClassementTitle = "Classement général";
			return View(listToDisplay);
        }

		public ActionResult TopLeadersGeneral()
		{
			StudentService studentService = new StudentService();

			// Récupération de la liste des étudiants
			List<Student> studentList = new List<Student>();
			studentList = studentService.Get().Select(c => c.ToStudentModel()).ToList();

			// Tri des étudiants qui sont inscrit à un ou plusieurs cours
			List<Student> listToDisplay = new List<Student>();

			foreach (var item in studentList)
			{
				if (item.StudentCourses.Count() > 0)
				{
					listToDisplay.Add(item);

					// Si l'étudiant est connecté, son profil ressort du classement
					if (UserSession.User != null && UserSession.User.Id == item.User.Id)
					{
						item.CssClass = "TopLearnerCurrentStudent"; 
					}
				}
			}

			listToDisplay.OrderBy(t => t.TotalPoints);
			ViewBag.ClassementTitle = "Classement général";
			return PartialView("_FilterAllCourses", listToDisplay);
		}

		// Logos des cours pour filtrer le classement des étudiants par cours
		public ActionResult CourseFilterMenu()
		{
			CourseService courseService = new CourseService();
			IEnumerable<Course> courses = courseService.GetCoursesWithSignedInStudents().Select(c => c.ToCourseModel());
			ViewBag.GeneralImgId = 22;
			return PartialView("_CourseFilterMenu", courses);
		}

		public ActionResult FilterByCourse(int courseId)
		{
			StudentService studentService = new StudentService();

			// Récupération de la liste des étudiants qui suivent le cours
			IEnumerable<Student> studentList = studentService.GetStudentsSignedInForCourse(courseId).Select(c => c.ToStudentModel()).ToList();

			foreach (var item in studentList)
			{
				item.Points = item.GetPointsByCourse(courseId);

				// Si l'étudiant est connecté, son profil ressort du classement
				if (UserSession.User != null && UserSession.User.Id == item.User.Id)
				{
					item.CssClass = "TopLearnerCurrentStudent";
				}
			}

			CourseService courseService = new CourseService();
			ViewBag.ClassementTitle = string.Format("Classement : {0}", courseService.Get(courseId).ToCourseModel().Title);
			ViewBag.CourseId = courseId;
			return PartialView("_FilterByCourse", studentList);
		}

    }
}