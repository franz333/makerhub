﻿using DAL.Services;
using Europe.Mapper;
using Europe.Models;
using Europe.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Europe.Controllers
{
    [AuthRequired]
    public class StudentController : Controller
    {
		[HttpGet]
		[AllowAnonymous]
		public ActionResult Get(int id)
		{
            StudentService studentService = new StudentService();
			Student student = new Student();
			student = studentService.Get(id).ToStudentModel();

			if (student.StudentCourses != null)
			{
				ViewBag.StudentPoint = student.StudentCourses.Sum(c => c.Points);
			}
			else
			{
				ViewBag.StudentPoint = 0;
			}

			// ViewBags
			ViewBag.Date = "Actif depuis : ";
			//ViewBag.ImgSrc = imageDataURL;
			return View(student);
		}

		[HttpGet]
		public ActionResult Update(int id)
		{
			// Le CTO du StudentUpdateForm instancie un student et récupère ses data en les insérant dans le form
			StudentUpdateForm form = new StudentUpdateForm(id);
            //ChangePwd changePwd = new ChangePwd();
            form.IsFlagSet = false;
            return View(form);
		}

		[HttpPost]
		public ActionResult Update(StudentUpdateForm form)
		{
			if (!ModelState.IsValid)
			{
				form.IsFlagSet = true;
				return View(form);
			}
			else
			{
				// Si un fichier est chargé par l'utilisateur on traite l'image...

				if (form.File != null && form.File.ContentType.Contains("image"))
				{
					string[] formats = new string[] { ".jpg", ".png", ".gif", ".jpeg" };

					if (formats.Any(item => form.File.FileName.EndsWith(item, StringComparison.OrdinalIgnoreCase)))
					{
						// Méta données de l'image 

						string alt = "avatar de " + form.Name + ", étudiant de Europe&Un";
						string imgFormat = Path.GetExtension(form.File.FileName);
						string name = Guid.NewGuid().ToString();

						// Encodage de l'image en byte array

						byte[] data;
						using (Stream inputStream = form.File.InputStream)
						{
							MemoryStream memoryStream = inputStream as MemoryStream;
							if (memoryStream == null)
							{
								memoryStream = new MemoryStream();
								inputStream.CopyTo(memoryStream);
							}
							data = memoryStream.ToArray();

							ImgService serv = new ImgService();

							// Si l'utilisateur ajoute son image custom, on l'insère dans la table Img et on update la table Student
							// Sinon s'il update une image custom déjà présente, on update celle-ci au même id

							if (form.ImgId == 2)
							{
								form.ImgId = serv.Insert(new DAL.Data.Img()
								{
									Data = data,
									Format = imgFormat,
									Name = name,
									Size = form.File.ContentLength,
									Alt = alt,
								});
								StudentService studentService = new StudentService();
								studentService.UpdateStudentImgId(new DAL.Data.Student()
								{
									Id = form.Id,
									ImgId = form.ImgId
								});
							}
							else
							{
								serv.Update(new DAL.Data.Img()
								{
									Id = form.ImgId,
									Data = data,
									Format = imgFormat,
									Name = name,
									Size = form.File.ContentLength,
									Alt = alt,
								});
							}
						}
					}
					else
					{
						// avertir l'utilisateur que le format n'est pas adéquant, déjà le faire côté client
						return View(form);
					}
				}
				else
				{
					// Si aucun fichier n'est chargé, il faut vérifier
				}
			}
			UserService userService = new UserService();
			userService.Update(new DAL.Data.User()
			{				
				Name = form.Name,
				Email = form.Email,
				Pwd = form.CurrentPwd,
				Id = form.UserId,
			});
			return RedirectToAction("Get", "Student", new { id = form.Id });
		}
	}
}