﻿using DAL.Services;
using Europe.Mapper;
using Europe.Models;
using Europe.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Europe.Custom_Helpers;

namespace Europe.Controllers
{
    [AuthRequired]
    public class CourseController : Controller
    {
        // GET: Course
        [AllowAnonymous]
        public ActionResult Index()
		{
            // Récupération des cours
			CourseService courseService = new CourseService();
			IEnumerable<Course> courses = courseService.Get().Select(c => c.ToCourseModel()).ToList();

			return View(courses);
		}

        [AllowAnonymous]
        public ActionResult CourseDetails(int courseId)
        {
            Course course = new Course();
            CourseService courseService = new CourseService();
            course = courseService.Get(courseId).ToCourseModel();

            // Le user est connecté
            if (UserSession.User != null)
            {
                // si le user est un student 
                if (UserSession.User.RoleType == 2)
                {
                    // On vérifie si le student est inscrit au cours et on gère l'affichage dans la vue via un CTA
                    int studentId = UserSession.User.StudentId;

                    StudentCourse studentCourse = new StudentCourse();
                    studentCourse = courseService.GetCoursesByStudentId(studentId).Select(c => c.ToStudentCourseModel()).Where(c => c.CourseId == courseId).SingleOrDefault();

                    if (studentCourse != null)
                    {
                        ViewBag.StudentCourseCount = 1;
                        ViewBag.Bookmark = studentCourse.Bookmark;
                    }
                    else
                    {
                        ViewBag.StudentCourseCount = 0;
                    }                    
                }
            }
            return View(course);
        }

		public string CourseInfoMenu(int courseId)
		{
			CourseService courseService = new CourseService();
			Course course = new Course();
			course = courseService.Get(courseId).ToCourseModel();
			return course.Title.ToString();
		}

        public ActionResult Learn(int courseId)
        {
			if (UserSession.User != null)
			{
				if (UserSession.User.RoleType == 2)
				{
					// On vérifie si le student est inscrit au cours 
					int stuId = UserSession.User.StudentId;
					StudentCourse stuCourse = new StudentCourse();
					CourseService service = new CourseService();
					stuCourse = service.GetCoursesByStudentId(stuId).Select(c => c.ToStudentCourseModel()).Where(c => c.CourseId == courseId).SingleOrDefault();
					if (stuCourse != null)
					{
						ViewBag.StudentPoints = stuCourse.Points;
						Course course = new Course();
						course = service.Get(courseId).ToCourseModel();
						return View("Learn", course);
					}
					else
					{
						return RedirectToAction("CourseDetails", "Course", new { courseId });
					}
				}
				else
				{
					ViewBag.Message = "Il faut un compte Etudiant pour suivre un cours, veuillez en créer un";
					return RedirectToAction("Add", "Student");
				}
			}
			return RedirectToAction("Login", "Account", new { courseId });
        }

		public ActionResult TakeUp(int courseId)
        {
            // On vérifie si le student est inscrit au cours 
            int studentId = UserSession.User.StudentId;
            StudentCourse stuCourse = new StudentCourse();
            CourseService service = new CourseService();
            stuCourse = service.GetCoursesByStudentId(studentId).Select(c => c.ToStudentCourseModel()).Where(c => c.CourseId == courseId).SingleOrDefault();

            if (stuCourse != null)
            {
                ViewBag.StudentPoints = stuCourse.Points;
                Course course = new Course();
                course = service.Get(courseId).ToCourseModel();
                return RedirectToAction ("Learn", "Course", new { courseId });
            }
            else
            {
                // S'il ne l'est pas, on l'inscrit et on le redirige
                stuCourse.StudentId = studentId;
                stuCourse.CourseId = courseId;
                stuCourse.BeginDate = DateTime.Now;

                CourseService courseService = new CourseService();
                courseService.Insert(stuCourse.ToStudentCourseDal());

                return RedirectToAction("Learn", "Course", new { courseId });
            }
        }

		public ActionResult ChaptersPartialViews(int courseId)
		{
			Course course = new Course();
			CourseService courseService = new CourseService();
			course = courseService.Get(courseId).ToCourseModel();
			return PartialView("_Chapters", course);
		}

        public ActionResult Chapter(int chapterId)
		{
			Chapter chapter = new Chapter();
			ChapterService chapterService = new ChapterService();
			chapter = chapterService.Get(chapterId).ToChapterModel();
			return PartialView("_ChapterDetails", chapter);
		}
			   
		public ActionResult ChapterInfoMenu(int chapterId)
		{
			Chapter chapter = new Chapter();
			ChapterService chapterService = new ChapterService();
			chapter = chapterService.Get(chapterId).ToChapterModel();
			return PartialView("_ChapterInfoMenu", chapter);
		}

		public ActionResult Lesson(int lessonId)
        {
            LessonService lessonService = new LessonService();
            Lesson lesson = lessonService.Get(lessonId).ToLessonModel();
            return PartialView("_LessonDetails", lesson);
        }

		public string LessonInfoMenu(int lessonId)
		{
			LessonService lessonService = new LessonService();
			Lesson lesson = lessonService.Get(lessonId).ToLessonModel();
			return lesson.Title.ToString();
		}

        public ActionResult Page(int pageId)
        {
			Page page = new Page();
			PageService pageService = new PageService();
			page = pageService.Get(pageId).ToPageModel();
            return PartialView("_PageDetails", page);
        }
		
		public ActionResult CoursesInsideMainView()
		{
			CourseService courseService = new CourseService();
			IEnumerable<Course> courses = courseService.Get().Select(c => c.ToCourseModel()).ToList();
			return PartialView("_CoursesInsideMainView", courses);
		}

		public ActionResult CourseItemInsideMainView(int id)
		{
			Course course = new Course();
			CourseService courseService = new CourseService();
			course = courseService.Get(id).ToCourseModel();
			return PartialView("_CourseItemInsideMainView", course);
		}

		//public ActionResult DisplayStudentCourses(int id)
		//{
		//	IEnumerable<StudentCourse> courses = new List<StudentCourse>();

		//	CourseService courseService = new CourseService();
		//	courses = courseService.GetCoursesByStudentId(id).Select(c => c.ToStudentCourseModel()).ToList();
		//	return PartialView(courses);
		//}
	}
}