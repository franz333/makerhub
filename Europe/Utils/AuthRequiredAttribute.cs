﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Europe.Utils
{
	// Redéfinir via un Attribut quelles méthodes, voir controllers nous limiterons les actions utilisateurs.
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
	public class AuthRequiredAttribute : AuthorizeAttribute
	{
		protected override bool AuthorizeCore(HttpContextBase httpContext)
		{
			return UserSession.User != null;
		}

		//protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
		//{
		//	filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { Controller = "Account", Action = "Login" }));
		//}

		protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
		{
			filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(
				new
				{
					Controller = "Account",
					Action = "Login",
					area = "",
					returnUrl = filterContext.HttpContext.Request.Url?.GetComponents(UriComponents.PathAndQuery, UriFormat.SafeUnescaped)
				}));
		}
	}
}