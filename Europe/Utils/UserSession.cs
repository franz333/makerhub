﻿using Europe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Europe.Utils
{

	// Création d'une classe tampon qui accédera aux variables sessions utilisateurs
	public static class UserSession
	{
		public static User User
		{
			get => (User)HttpContext.Current.Session["User"];
			set => HttpContext.Current.Session["User"] = value;
		}
	}
}


