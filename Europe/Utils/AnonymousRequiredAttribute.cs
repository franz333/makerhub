﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Europe.Utils
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
	public class AnonymousRequiredAttribute : AuthorizeAttribute
	{
		protected override bool AuthorizeCore(HttpContextBase httpContext)
		{
			return UserSession.User == null;
		}

		protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
		{
			filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { Controller = "Student", Action = "Details", Id = UserSession.User.Id }));
		}
	}
}