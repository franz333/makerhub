﻿using DAL.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{
	interface IStudentBadgeRepository : IRepository<StudentBadge, int>
	{
	}
}
