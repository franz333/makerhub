﻿using DAL.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Mapper
{
	public static class Mapper
	{
		public static User ToUser(this IDataRecord record) => new User()
		{
			Id = (int)record["Id"],
			Date = (DateTime)record["AccountDate"],
			Name = record["Name"].ToString(),
			Email = record["Email"].ToString(),
			Pwd = record["Pwd"].ToString(),
			Active = (bool)record["Active"],
			Deleted = (bool)record["Deleted"],
			RoleType = (int)record["Role_Type"],
            Token = record["Token"] == DBNull.Value ? null : (Guid?)record["Bookmark"],
        };

		public static StudentCourse ToStudentCourse(this IDataRecord record)
		{
            return new StudentCourse
            {
                BeginDate = (DateTime)record["BeginDate"],
                CourseId = (int)record["Course_Id"],
                CourseImgId = (int)record["Img_Id"],
                CourseTitle = record["Title"].ToString(),
                Points = (int)record["Points"],
                StudentId = (int)record["Student_Id"],
                Bookmark = record["Bookmark"].ToString(),
			};
		}

		public static Course ToCourse(this IDataRecord record)
		{
			return new Course
			{
				Id = (int)record["Id"],
				Title = record["Title"].ToString(),
				Description = record["Description"].ToString(),
				Level = record["Difficulty"].ToString(),
				Active = (bool)record["Active"],
				Deleted = (bool)record["Deleted"],
				CreationDate = (DateTime)record["CreationDate"],
				UpdateDate = record["UpdateDate"] == DBNull.Value ? null : (DateTime?)record["UpdateDate"],
				PublicationDate = record["PublicationDate"] == DBNull.Value ? null : (DateTime?)record["PublicationDate"],
				IdCreator = (int)record["User_Id_Creator"],
				IdPublisher = record["User_Id_Publisher"] == DBNull.Value ? null : (int?)record["User_Id_Publisher"],
				CertificateId = (int)record["Certificate_Id"],
				ImgId = (int)record["Img_Id"],
			};
		}

		public static Chapter ToChapter(this IDataRecord record)
		{
			return new Chapter
			{
				Id = (int)record["Id"],
				Num = (int)record["Num"],
				Title = record["Title"].ToString(),
				Active = (bool)record["Active"],
				Deleted = (bool)record["Deleted"],
				CourseId = (int)record["Course_Id"],
				ImgId = (int)record["Img_Id"],
			};
		}

		public static Certificate ToCertificate(this IDataRecord record)
		{
			return new Certificate
			{
				Id = (int)record["Id"],
				Title = record["Title"].ToString(),
				Description = record["Description"].ToString(),
				Difficulty = record["Difficulty"].ToString(),
				Active = (bool)record["Active"],
				ImgId = (int)record["Img_Id"]
			};
		}

		public static Lesson ToLesson(this IDataRecord record)
		{
			return new Lesson
			{
				Id = (int)record["Id"],
				Num = (int)record["Num"],
				Title = record["Title"].ToString(),
				ChapterId = (int)record["Chapter_Id"],
				Active = (bool)record["Active"],
				Deleted = (bool)record["Deleted"],
				ImgId = (int)record["Img_Id"],
			};
		}

		public static Page ToPage(this IDataRecord record)
		{
			return new Page
			{
				Id = (int)record["Id"],
				Num = (int)record["Num"],
				Title = record["Title"].ToString(),
				Content = record["Content"].ToString(),
				IsQuizz = (bool)record["IsQuizz"],
				Active = (bool)record["Active"],
				Deleted = (bool)record["Deleted"],
				ChapterId = (int)record["Chapter_Id"],
				LessonId = (int)record["Lesson_Id"],
			};
		}

		public static QuizzField ToQuizzField(this IDataRecord record)
		{
			return new QuizzField
			{
				Active = (bool)record["Active"],
				Deleted = (bool)record["Deleted"],
				IsAnswer = (bool)record["IsAnswer"],
				Id = (int)record["Id"],
				PageId = (int)record["Page_Id"],
				Content = record["Content"].ToString(),
			};
		}

		public static Student ToStudent(this IDataRecord record)
		{
			return new Student
			{
				Id = (int)record["Id"],
				UserId = (int)record["User_Id"],
				ImgId = (int)record["Img_Id"],
			};
		}

		public static Img ToImg(this IDataRecord record)
		{
			return new Img
			{
				Id = (int)record["Id"],
				Data = (byte[])record["Data"],
				Format = record["Format"].ToString(),
				Name = record["Name"].ToString(),
				Size = (int)record["Size"],
				Title = record["Title"].ToString(),
				Alt = record["Alt"].ToString(),
				Caption = record["Caption"].ToString(),
				IsAdmin = (bool)record["IsAdmin"],
			};
		}

		public static Role ToRole(this IDataRecord record) => new Role()
		{
			Id = (int)record["Id"],
			Type = record["Type"].ToString(),
		};

		public static Badge ToBadge(this IDataRecord record) => new Badge()
		{
			Id = (int)record["Id"],
			Title = record["Title"].ToString(),
			Description = record["Description"].ToString(),
			Instruction = record["Instruction"].ToString(),
			Active = (bool)record["Active"],
			Deleted = (bool)record["Deleted"],
			ImgId = (int)record["Img_Id"],
		};

		public static StudentBadge ToStudentBadge(this IDataRecord record) => new StudentBadge()
		{
			BadgeId = (int)record["Badge_Id"],
			Date = (DateTime)record["Date"],
			StudentId = (int)record["Student_Id"],
		};

		//#region A voir plus tard 

		//public static App ToApp(this IDataRecord record) => new App()
		//{
		//	Id = (int)record["Id"],
		//	Title = record["Title"].ToString(),
		//	Baseline = record["Baseline"].ToString(),
		//	FbURL = record["FB_URL"].ToString(),
		//	TwitterURL = record["Twitter_URL"].ToString(),
		//	Credits = record["Credits"].ToString(),
		//	ImgId = (int)record["Img_id"],
		//};

		//public static Prof ToProf(this IDataRecord record)
		//{
		//	return new Prof
		//	{
		//		Id = (int)record["Id"],
		//		Name = record["Name"].ToString(),
		//	};
		//}
		//#endregion
	}
}
