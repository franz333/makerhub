﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Data
{
	/// <summary>
	/// Cette classe regroupe des informations de deux tables pour limiter les requêtes
	/// </summary>
	public class StudentCourse
	{
        public DateTime BeginDate { get; set; }
		public int Points { get; set; }
		public int CourseId { get; set; }
		public string CourseTitle { get; set; }
		public int CourseImgId { get; set; }
        public int StudentId { get; set; }
        public string Bookmark { get; set; }
	}
}
