﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Data
{
	public class Badge
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public string Instruction { get; set; }
		public bool Active { get; set; }
		public bool Deleted { get; set; }
		public int ImgId { get; set; }
	}
}
