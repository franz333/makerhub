﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Data
{
	public class Chapter
	{
		public int Id { get; set; }
		public int Num { get; set; }
		public string Title { get; set; }
		public bool Active { get; set; }
		public bool Deleted { get; set; }
		public int CourseId { get; set; }
		public int ImgId { get; set; }
	}
}
