﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Data
{
	public class Img
	{
		public int Id { get; set; }
		public byte[] Data { get; set; }
		public string Format { get; set; }
		public string Name { get; set; }
		public int Size { get; set; }
		public string Title { get; set; }
		public string Alt { get; set; }
		public string Caption { get; set; }
		public bool IsAdmin { get; set; }
	}
}
