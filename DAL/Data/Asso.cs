﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Data
{
	public class Asso
	{
		public int Id { get; set; }
		public int UserId { get; set; }
		public string Street { get; set; }
		public string Num { get; set; }
		public string Box { get; set; }
		public string City { get; set; }
		public string PostCode { get; set; }
		public string Country { get; set; }
		public string Website { get; set; }
		public int Email { get; set; }
		public string Description { get; set; }
		public int PIC { get; set; }
		public string Sector { get; set; }
		public bool Active { get; set; }
		public bool Deleted { get; set; }
		public int ImgId { get; set; }
	}
}
