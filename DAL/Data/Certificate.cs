﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Data
{
	public class Certificate
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public string Difficulty { get; set; }
		public bool Active { get; set; }
		public int ImgId { get; set; }
	}
}
