﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Data
{
	public class StudentBadge
	{
		public DateTime Date { get; set; }
		public int StudentId { get; set; }
		public int BadgeId { get; set; }
	}
}
