﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Data
{
	public class Student
	{
		public int Id { get; set; }
		public int UserId { get; set; }
		public int ImgId { get; set; }
	}
}
