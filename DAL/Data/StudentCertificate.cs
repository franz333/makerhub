﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Data
{
	public class StudentCertificate
	{
		public DateTime Date { get; set; }
		public string Pdf { get; set; }
		public int StudentId { get; set; }
		public int CertificateId { get; set; }
	}
}
