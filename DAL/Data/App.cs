﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Data
{
	public class App
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public string Baseline { get; set; }
		public string FbURL { get; set; }
		public string TwitterURL { get; set; }
		public string Credits { get; set; }
		public int ImgId { get; set; }
	}
}
