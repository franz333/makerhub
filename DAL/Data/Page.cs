﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Data
{
	public class Page
	{
		public int Id { get; set; }
		public int Num { get; set; }
		public string Title { get; set; }
		public string Content { get; set; }
		public bool IsQuizz { get; set; }
		public bool Active { get; set; }
		public bool Deleted { get; set; }
		public int ChapterId { get; set; }
		public int LessonId { get; set; }
	}
}
