﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Data
{
	public class Course
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public string Level { get; set; }
		public bool Active { get; set; }
		public bool Deleted { get; set; }
		public DateTime CreationDate { get; set; }
		public DateTime? UpdateDate { get; set; }
		public DateTime? PublicationDate { get; set; }
		public int IdCreator { get; set; }
		public int? IdPublisher { get; set; }
		public int CertificateId { get; set; }
		public int ImgId { get; set; }
	}
}
