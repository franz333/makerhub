﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Data
{
	public class User
	{
		public int Id { get; set; }
		public DateTime Date { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public string Pwd { get; set; }
		public bool Active { get; set; }
		public bool Deleted { get; set; }
		public int RoleType { get; set; }
        public Guid? Token { get; set; }
	}
}
