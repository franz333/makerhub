﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Data
{
	public class QuizzField
	{
		public int Id { get; set; }
		public string Content { get; set; }
		public int PageId { get; set; }
		public bool Active { get; set; }
		public bool Deleted { get; set; }
		public bool IsAnswer { get; set; }
	}
}
