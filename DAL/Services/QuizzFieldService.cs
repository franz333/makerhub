﻿using DAL.Data;
using DAL.Mapper;
using DAL.Repository;
using DBConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Services
{
	public class QuizzFieldService : IQuizzFieldRepository
	{
		string connexionString = DBConfig.ConnexionString;

		public bool Delete(int id)
		{
			throw new NotImplementedException();
		}

		public IEnumerable<QuizzField> Get()
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("select * from QuizzField;", false);
			return con.ExecuteReader<QuizzField>(cmd, reader => reader.ToQuizzField());
		}

		public IEnumerable<QuizzField> GetByPageId(int pageId)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("select * from QuizzField where Page_Id = @pageId;", false);
			cmd.AddParameter("@pageId", pageId);
			return con.ExecuteReader<QuizzField>(cmd, reader => reader.ToQuizzField());
		}

		public QuizzField Get(int id)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("select * from QuizzField where Id = @id", false);
			cmd.AddParameter("@id", id);
			return con.ExecuteReader<QuizzField>(cmd, reader => reader.ToQuizzField()).SingleOrDefault();
		}

		public int Insert(QuizzField entity)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("insert into QuizzField (Content, Page_Id, Active, Deleted, IsAnswer) output inserted.Id values (@content, @pageId, @active, @deleted, @isAnswer)", false);
			cmd.AddParameter("@content", entity.Content);
			cmd.AddParameter("@pageId", entity.PageId);
			cmd.AddParameter("@active", entity.Active);
			cmd.AddParameter("@deleted", entity.Deleted);
			cmd.AddParameter("@isAnswer", entity.IsAnswer);
			return (int)con.ExecuteScalar(cmd);
		}

		public bool Update(QuizzField entity)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("update QuizzField Set Content = @content, Page_Id = @pageId, Active = @active, Deleted = @deleted, IsAnswer = @isAnswer WHERE Id = @id", false);
			cmd.AddParameter("@content", entity.Content);
			cmd.AddParameter("@pageId", entity.PageId);
			cmd.AddParameter("@active", entity.Active);
			cmd.AddParameter("@deleted", entity.Deleted);
			cmd.AddParameter("@isAnswer", entity.IsAnswer);
			cmd.AddParameter("@id", entity.Id);
			return (int)con.ExecuteNonQuery(cmd) == 1;
		}
	}
}
