﻿using DAL.Data;
using DAL.Mapper;
using DAL.Repository;
using DBConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Services
{
	public class ChapterService : IChapterRepository
	{
		string connexionString = DBConfig.ConnexionString;

		public bool Delete(int id)
		{
			throw new NotImplementedException();
		}

		public IEnumerable<Chapter> Get()
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("select * from Chapter", false);
			return con.ExecuteReader<Chapter>(cmd, reader => reader.ToChapter());
		}

		public Chapter Get(int id)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("select * from Chapter where id = @id", false);
			cmd.AddParameter("@id", id);
			return con.ExecuteReader<Chapter>(cmd, reader => reader.ToChapter()).FirstOrDefault();
		}

		public IEnumerable<Chapter> GetChaptersByCourse(int courseId)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("select * from Chapter where Course_Id = @courseId", false);
			cmd.AddParameter("@courseId", courseId);
			return con.ExecuteReader<Chapter>(cmd, reader => reader.ToChapter());
		}

		public int Insert(Chapter chapter)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("insert into Chapter (Num, Title, Active, Deleted, Course_Id, Img_Id) output inserted.Id " +
				"values (@num, @title, @active, @deleted, @courseId, @imgId)", false);
			cmd.AddParameter("@num", chapter.Num);
			cmd.AddParameter("@title", chapter.Title);
			cmd.AddParameter("@active", chapter.Active);
			cmd.AddParameter("@deleted", chapter.Deleted);
			cmd.AddParameter("@courseId", chapter.CourseId);
			cmd.AddParameter("@imgId", chapter.ImgId);
			return (int)con.ExecuteScalar(cmd);
		}

		public bool Update(Chapter chapter)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("update Chapter Set Title = @title, Img_Id = @imgId where Id = @id", false);
			cmd.AddParameter("@id", chapter.Id);
			cmd.AddParameter("@title", chapter.Title);
			cmd.AddParameter("@imgId", chapter.ImgId);
			return con.ExecuteNonQuery(cmd) == 1;
		}

		public bool UpdateChapterOrder(Chapter chapter)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("update Chapter Set Num = @num where Id = @id", false);
			cmd.AddParameter("@num", chapter.Num);
			cmd.AddParameter("@id", chapter.Id);
			return con.ExecuteNonQuery(cmd) == 1;
		}
	}
}
