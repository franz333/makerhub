﻿using DAL.Data;
using DAL.Mapper;
using DAL.Repository;
using DBConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Services
{
	public class CertificateService : ICertificateRepository
	{
		string connexionString = DBConfig.ConnexionString;

		public bool Delete(int id)
		{
			throw new NotImplementedException();
		}

		public IEnumerable<Certificate> Get()
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("select * from Certificate", false);
			return con.ExecuteReader<Certificate>(cmd, reader => reader.ToCertificate());
		}

		public Certificate Get(int id)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("Select * from Certificate where Id = @id", false);
			cmd.AddParameter("@id", id);
			return con.ExecuteReader<Certificate>(cmd, reader => reader.ToCertificate()).SingleOrDefault();
		}

		public int Insert(Certificate certificate)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("Insert into Certificate (Title, Description, Difficulty, Active) output inserted.Id values(@title, @description, @difficulty, @active)", false);
			cmd.AddParameter("@title", certificate.Title);
			cmd.AddParameter("@description", certificate.Description);
			cmd.AddParameter("@difficulty", certificate.Difficulty);
			cmd.AddParameter("@active", certificate.Active);
			return (int)con.ExecuteScalar(cmd);
		}

		// TODO : changer Img_Id pour ImgId
		public bool Update(Certificate certificate)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("Update Certificate Set Title = @title, Description = @description, Img_Id = @imgId where Id = @id", false);
			cmd.AddParameter("@id", certificate.Id);
			cmd.AddParameter("@title", certificate.Title);
			cmd.AddParameter("@description", certificate.Description);
			cmd.AddParameter("@imgId", certificate.ImgId);
			return con.ExecuteNonQuery(cmd) == 1;
		}
	}
}
