﻿using DAL.Data;
using DAL.Mapper;
using DAL.Repository;
using DBConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Services
{
	public class UserService : IUserRepository
	{
		string connexionString = DBConfig.ConnexionString;

		public IEnumerable<User> Get()
		{
			//Connexion con = new Connexion(connexionString);
			//Command cmd = new Command("select * from v_UserRole", false); // TODO Ajouter la vue dans stored p.
			//return con.ExecuteReader<User>(cmd, reader => reader.ToUser());
			throw new NotImplementedException();
		}

		public bool Delete(int id)
		{
			throw new NotImplementedException();
		}

		public User Get(int id)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("select * from [User] where Id = @id", false);
			cmd.AddParameter("@id", id);
			return con.ExecuteReader<User>(cmd, reader => reader.ToUser()).FirstOrDefault();
		}

		public User GetByEmail(string email)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("select * from [User] where Email = @email", false);
			cmd.AddParameter("@email", email);
			return con.ExecuteReader<User>(cmd, reader => reader.ToUser()).FirstOrDefault();
		}

		public int Insert(User user)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("insert into [User] (Name, Email, Pwd, Role_Type) values (@name, @email, @pwd, @roleType); select top 1 [Id] from [User] order by [Id] desc;", false);
			cmd.AddParameter("@name", user.Name);
			cmd.AddParameter("@email", user.Email);
			cmd.AddParameter("@pwd", user.Pwd);
			cmd.AddParameter("@roleType", user.RoleType);
			return (int)con.ExecuteScalar(cmd);
		}

		public bool Update(User user)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("update [User] set Name = @name, Email = @email, Pwd = @pwd where Id = @id", false);
			cmd.AddParameter("@name", user.Name);
			cmd.AddParameter("@email", user.Email);
			cmd.AddParameter("@pwd", user.Pwd);
			cmd.AddParameter("@id", user.Id);
			return con.ExecuteNonQuery(cmd) > 0;

		}
	}
}
