﻿using DAL.Data;
using DAL.Mapper;
using DAL.Repository;
using DBConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Services
{
	public class StudentService : IStudentRepository
	{
		string connexionString = DBConfig.ConnexionString;

		public bool Delete(int id)
		{
			throw new NotImplementedException();
		}

		public IEnumerable<Student> Get()
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("select * from Student", false); // TODO : Ajouter Stored P
			return con.ExecuteReader<Student>(cmd, reader => reader.ToStudent());
		}

		// Récupération des étudiants inscrits à un cours donné
		public IEnumerable<Student> GetStudentsSignedInForCourse(int courseId)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("sp_GetStudentsSignedInForCourse");
			cmd.AddParameter("@courseId", courseId);
			return con.ExecuteReader<Student>(cmd, reader => reader.ToStudent());
		}

		public Student Get(int id)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("select * from Student where Id = @id", false);
			cmd.AddParameter("@id", id);
			return con.ExecuteReader<Student>(cmd, reader => reader.ToStudent()).FirstOrDefault();
		}

		public Student GetByUserId(int userId)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("select * from Student where User_Id = @userId", false);
			cmd.AddParameter("@userId", userId);
			return con.ExecuteReader<Student>(cmd, reader => reader.ToStudent()).FirstOrDefault();
		}

		// TODO : peut on avoir des méthodes de services non implémentées dans le cas où d'autres serices
		public int Insert(Student student)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("insert into Student ([User_Id], Img_Id) output inserted.Id values (@userId, @imgId)", false);
			cmd.AddParameter("@userId", student.UserId);
			cmd.AddParameter("@imgId", student.ImgId);
			return (int)con.ExecuteScalar(cmd);
		}

		public bool Update(Student entity)
		{
			throw new NotImplementedException();
		}

		public bool UpdateStudentImgId(Student entity)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("update Student SET Img_Id = @imgId where Id = @id", false);
			cmd.AddParameter("@imgId", entity.ImgId);
			cmd.AddParameter("@id", entity.Id);
			return con.ExecuteNonQuery(cmd) == 1;			
		}
	}
}
