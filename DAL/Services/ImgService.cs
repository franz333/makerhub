﻿using DAL.Data;
using DAL.Mapper;
using DAL.Repository;
using DBConnection;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Services
{
	public class ImgService : IImgRepository
	{
		string connexionString = DBConfig.ConnexionString;

		public bool Delete(int id)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("delete from Img where Id = @id", false);
			cmd.AddParameter("@id", id);
			return con.ExecuteNonQuery(cmd) > 0;
		}

		public IEnumerable<Img> Get()
		{
			// 10 images pour la gestion en Admin
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("select top 30 * from Img", false);
			return con.ExecuteReader<Img>(cmd, reader => reader.ToImg());
		}

		public Img Get(int id)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("select * from Img where Id = @id", false);
			cmd.AddParameter("@id", id);
			return con.ExecuteReader<Img>(cmd, reader => reader.ToImg()).FirstOrDefault();
		}

		public int Insert(Img img)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("Insert into Img (Data, Name, Size, Title, Alt, Caption, [Format], IsAdmin) output inserted.Id values (@data, @name, @size, @title, @alt, @caption, @format, @isAdmin)", false);
			cmd.AddParameter("@data", img.Data, DbType.Binary);
			cmd.AddParameter("@name", img.Name);
			cmd.AddParameter("@size", img.Size);
			cmd.AddParameter("@title", img.Title);
			cmd.AddParameter("@alt", img.Alt);
			cmd.AddParameter("@caption", img.Caption);
			cmd.AddParameter("@format", img.Format);
			cmd.AddParameter("@isAdmin", img.IsAdmin);
			return (int)con.ExecuteScalar(cmd);
		}

		public bool Update(Img img)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("Update Img set Name = @name, Data = @data, Size = @size, [Format] = @format, IsAdmin = @isAdmin where Id = @id", false);
			cmd.AddParameter("@name", img.Name);
			cmd.AddParameter("@data", img.Data, DbType.Binary);
			cmd.AddParameter("@size", img.Size);
			cmd.AddParameter("@format", img.Format);
			cmd.AddParameter("@isAdmin", img.IsAdmin);
			cmd.AddParameter("@id", img.Id);
			return con.ExecuteNonQuery(cmd) == 1;
		}

		public bool UpdateImgFields(Img img)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("Update Img set Name = @name, Title=@title, Alt=@alt, Caption=@caption where Id = @id", false);
			cmd.AddParameter("@name", img.Name);
			cmd.AddParameter("@title", img.Title);
			cmd.AddParameter("@alt", img.Alt);
			cmd.AddParameter("@caption", img.Caption);
			cmd.AddParameter("@id", img.Id);
			return con.ExecuteNonQuery(cmd) == 1;
		}

		public int InsertImage(Img img)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("Insert into Img (Data, Name, Size, Title, Alt, Caption, [Format]) output inserted.Id values (@data, @name, @size, @title, @alt, @caption, @format)", false);
			cmd.AddParameter("@data", img.Data, DbType.Binary);
			cmd.AddParameter("@name", img.Name);
			cmd.AddParameter("@size", img.Size);
			cmd.AddParameter("@title", img.Title);
			cmd.AddParameter("@alt", img.Alt);
			cmd.AddParameter("@caption", img.Caption);
			cmd.AddParameter("@format", img.Format);
			return (int)con.ExecuteScalar(cmd);
		}


	}
}
