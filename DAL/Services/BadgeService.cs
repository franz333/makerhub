﻿using DAL.Data;
using DAL.Mapper;
using DAL.Repository;
using DBConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Services
{
	public class BadgeService : IBadgeRepository
	{
		string connexionString = DBConfig.ConnexionString;

		public bool Delete(int id)
		{
			throw new NotImplementedException();
		}

		public IEnumerable<Badge> Get()
		{
			throw new NotImplementedException();
		}

		//public IEnumerable<BadgeStudent> GetByStudentId(int id)
		//{
		//	Connexion con = new Connexion(connexionString);
		//	Command cmd = new Command("select * from StudentBadge where Student_Id = @studentId", false);
		//	cmd.AddParameter("@studentId", id);
		//	return con.ExecuteReader<BadgeStudent>(cmd, reader => reader.ToBadgeStudent());
		//}

		public Badge Get(int id)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("select * from Badge where Id = @id", false);
			cmd.AddParameter("@id", id); 
			return con.ExecuteReader<Badge>(cmd, reader => reader.ToBadge()).SingleOrDefault();
		}

		public int Insert(Badge entity)
		{
			throw new NotImplementedException();
		}

		public bool Update(Badge entity)
		{
			throw new NotImplementedException();
		}
	}
}
