﻿using DAL.Data;
using DAL.Mapper;
using DAL.Repository;
using DBConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Services
{
	public class PageService : IPageRepository
	{
		string connexionString = DBConfig.ConnexionString;

		public bool Delete(int id)
		{
			throw new NotImplementedException();
		}

		public IEnumerable<Page> Get()
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("select * from Page", false);
			return con.ExecuteReader<Page>(cmd, reader => reader.ToPage());
		}

		public IEnumerable<Page> GetByLessonId(int lessonId)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("select * from Page where Lesson_Id = @lessonId", false);
			cmd.AddParameter("@lessonId", lessonId);
			return con.ExecuteReader<Page>(cmd, reader => reader.ToPage());
		}

		public Page Get(int id)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("select * from Page where Id = @id", false);
			cmd.AddParameter("@id", id);
			return con.ExecuteReader<Page>(cmd, reader => reader.ToPage()).SingleOrDefault();
		}

		public int Insert(Page page)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("insert into Page (Num, Title, Content, IsQuizz, Active, Deleted, Chapter_Id, Lesson_Id) output inserted.Id values (@num, @title, @content, @isQuizz, @active, @deleted, @chapterId, @lessonId)", false);
			cmd.AddParameter("@num", page.Num);
			cmd.AddParameter("@title", page.Title);
			cmd.AddParameter("@content", page.Content);
			cmd.AddParameter("@isQuizz", page.IsQuizz);
			cmd.AddParameter("@active", page.Active);
			cmd.AddParameter("@deleted", page.Deleted);
			cmd.AddParameter("@chapterId", page.ChapterId);
			cmd.AddParameter("@lessonId", page.LessonId);
			return (int)con.ExecuteScalar(cmd);
		}

		public bool Update(Page page)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("update Page Set Title = @title, Content = @content, IsQuizz = @isQuizz where Id = @id", false);
			cmd.AddParameter("@id", page.Id);
			cmd.AddParameter("@title", page.Title);
			cmd.AddParameter("@content", page.Content);
			cmd.AddParameter("@isQuizz", page.IsQuizz);
			return con.ExecuteNonQuery(cmd) == 1;
		}

		public bool UpdatePageOrder(Page page)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("update Page Set Num = @num, where Id = @id", false);
			cmd.AddParameter("#num", page.Num);
			cmd.AddParameter("@id", page.Id);
			return con.ExecuteNonQuery(cmd) == 1;
		}
	}
}
