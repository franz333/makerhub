﻿using DAL.Data;
using DAL.Mapper;
using DAL.Repository;
using DBConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Services
{
	public class StudentBadgeService : IStudentBadgeRepository
	{
		string connexionString = DBConfig.ConnexionString;

		public bool Delete(int id)
		{
			throw new NotImplementedException();
		}

		public IEnumerable<StudentBadge> GetByStudentId(int id)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("select * from StudentBadge where Student_Id = @studentId", false);
			cmd.AddParameter("@studentId", id);
			return con.ExecuteReader<StudentBadge>(cmd, reader => reader.ToStudentBadge());
		}

		public IEnumerable<StudentBadge> Get()
		{
			throw new NotImplementedException();
		}

		public StudentBadge Get(int id)
		{
			throw new NotImplementedException();
		}

		public int Insert(StudentBadge entity)
		{
			throw new NotImplementedException();
		}

		public bool Update(StudentBadge entity)
		{
			throw new NotImplementedException();
		}
	}
}
