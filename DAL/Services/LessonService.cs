﻿using DAL.Data;
using DAL.Mapper;
using DAL.Repository;
using DBConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Services
{
	public class LessonService : ILessonRepository
	{
		string connexionString = DBConfig.ConnexionString;

		public bool Delete(int id)
		{
			throw new NotImplementedException();
		}

		public IEnumerable<Lesson> Get()
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("select * from Lesson", false);
			return con.ExecuteReader<Lesson>(cmd, reader => reader.ToLesson());
		}

		public IEnumerable<Lesson> GetByChapter(int chapterId)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("select * from Lesson where Chapter_Id = @chapterId", false);
			cmd.AddParameter("@chapterId", chapterId);
			return con.ExecuteReader<Lesson>(cmd, reader => reader.ToLesson());
		}

		public Lesson Get(int id)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("select * from Lesson where Id = @id", false);
			cmd.AddParameter("@id", id);
			return con.ExecuteReader<Lesson>(cmd, reader => reader.ToLesson()).FirstOrDefault();
		}

		public int Insert(Lesson lesson)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("insert into Lesson (Title, Num, Chapter_Id) output inserted.Id values (@title, @num, @chapterId)", false);
			cmd.AddParameter("@title", lesson.Title);
			cmd.AddParameter("@num", lesson.Num);
			cmd.AddParameter("@chapterId", lesson.ChapterId);
			return (int)con.ExecuteScalar(cmd);
		}

		public bool Update(Lesson lesson)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("update Lesson Set Title = @title where Id = @id", false);
			cmd.AddParameter("@title", lesson.Title);
			cmd.AddParameter("@id", lesson.Id);
			return con.ExecuteNonQuery(cmd) == 1;
		}

		public bool UpdateLessonOrder(Lesson lesson)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("update Lesson Set Num = @num where Id = @id", false);
			cmd.AddParameter("@num", lesson.Num);
			cmd.AddParameter("@id", lesson.Id);
			return con.ExecuteNonQuery(cmd) == 1;
		}
	}
}
