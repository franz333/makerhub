﻿using DAL.Data;
using DAL.Mapper;
using DAL.Repository;
using DBConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Services
{
	public class RoleService : IRoleRepository
	{
		string connexionString = DBConfig.ConnexionString;

		public bool Delete(int id)
		{
			throw new NotImplementedException();
		}

		public IEnumerable<Role> Get()
		{
			throw new NotImplementedException();
		}

		public Role Get(int id)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("Select * from Role where Id = @id", false);
			cmd.AddParameter("@id", id);
			return con.ExecuteReader<Role>(cmd, reader => reader.ToRole()).FirstOrDefault();
		}

		public int Insert(Role entity)
		{
			throw new NotImplementedException();
		}

		public bool Update(Role entity)
		{
			throw new NotImplementedException();
		}
	}
}
