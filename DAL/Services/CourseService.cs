﻿using DAL.Data;
using DAL.Mapper;
using DAL.Repository;
using DBConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Services
{
	public class CourseService : ICourseRepository
	{
		string connexionString = DBConfig.ConnexionString;

		public bool Delete(int id)
		{
			throw new NotImplementedException();
		}

		public IEnumerable<Course> Get()
		{
            Connexion con = new Connexion(connexionString);
            Command cmd = new Command("select * from Course where Active = 1 and Deleted = 0;", false);
            return con.ExecuteReader<Course>(cmd, reader => reader.ToCourse());
        }

		public IEnumerable<Course> GetCoursesWithSignedInStudents()
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("select * from v_CoursesWithSignedInStudents;", false);
			return con.ExecuteReader<Course>(cmd, reader => reader.ToCourse());
		}

		public Course Get(int id)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("select * from Course where Id = @id;", false);
			cmd.AddParameter("@id", id);
			return con.ExecuteReader<Course>(cmd, reader => reader.ToCourse()).SingleOrDefault();
		}

		public int Insert(Course course)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("insert into Course (Title, Description, Difficulty, Active, Deleted, CreationDate, UpdateDate, PublicationDate, User_Id_Creator, User_Id_Publisher, Certificate_Id, Img_Id) output inserted.Id values (@title, @description, @difficulty, @active, @deleted, @creationDate, @updateDate, @publicationDate, @userIdCreator, @userIdPublisher, @certificateId, @imgId)", false);
			cmd.AddParameter("@title", course.Title);
			cmd.AddParameter("@description", course.Description);
			cmd.AddParameter("@difficulty", course.Level);
			cmd.AddParameter("@active", course.Active);
			cmd.AddParameter("@deleted", course.Deleted);
			cmd.AddParameter("@creationDate", course.CreationDate);
			cmd.AddParameter("@updateDate", course.UpdateDate);
			cmd.AddParameter("@publicationDate", course.PublicationDate);
			cmd.AddParameter("@userIdCreator", course.IdCreator);
			cmd.AddParameter("@userIdPublisher", course.IdPublisher);
			cmd.AddParameter("@certificateId", course.CertificateId);
			cmd.AddParameter("@imgId", course.ImgId);
			return (int)con.ExecuteScalar(cmd);
		}

		public bool Update(Course course)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("update Course Set Title = @title, Description = @description, Difficulty = @difficulty, Active = @active, Deleted = @deleted, UpdateDate = @updateDate, Img_Id = @imgId WHERE Id = @id", false);
            cmd.AddParameter("@title", course.Title);
			cmd.AddParameter("@description", course.Description);
			cmd.AddParameter("@difficulty", course.Level);
			cmd.AddParameter("@active", course.Active);
			cmd.AddParameter("@deleted", course.Deleted);
			cmd.AddParameter("@updateDate", course.UpdateDate);
            cmd.AddParameter("@imgId", course.ImgId);
			cmd.AddParameter("@id", course.Id);
			return (int)con.ExecuteNonQuery(cmd) == 1;
		}

		//public IEnumerable<StudentCourse> GetStudentCourses()
		//{
		//	Connexion con = new Connexion(connexionString);
		//	Command cmd = new Command("select * from StudentCourse", false);
		//	return con.ExecuteReader<StudentCourse>(cmd, reader => reader.ToStudentCourse());
		//}

		public IEnumerable<StudentCourse> GetCoursesByStudentId(int id)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("select * from v_StudentCourse where Student_Id = @id;", false);
			cmd.AddParameter("@id", id);
			return con.ExecuteReader<StudentCourse>(cmd, reader => reader.ToStudentCourse());
		}

		// Récupération d'un student
		public StudentCourse GetByStudentIdAndCourseId(int studentId, int courseId)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("select * from v_StudentCourse where Student_Id = @studentId and Course_Id = @courseId", false);
			cmd.AddParameter("@studentId", studentId);
			cmd.AddParameter("@courseId", courseId);
			return con.ExecuteReader<StudentCourse>(cmd, reader => reader.ToStudentCourse()).FirstOrDefault();
		}

		public StudentCourse GetCourseByStudentId(int studentId, int courseId)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("select * from v_StudentCourse where Student_Id = @studentId and Course_Id = @courseId;", false);
			cmd.AddParameter("@studentId", studentId);
			cmd.AddParameter("@courseId", courseId);
			return con.ExecuteReader<StudentCourse>(cmd, reader => reader.ToStudentCourse()).SingleOrDefault();
		}

		public int Insert(StudentCourse studentCourse)
		{
			Connexion con = new Connexion(connexionString);
			Command cmd = new Command("insert into StudentCourse (BeginDate, Student_Id, Course_Id) output inserted.Course_Id values (@date, @studentId, @courseId);", false);
			cmd.AddParameter("@date", DateTime.Now);
			cmd.AddParameter("@studentId", studentCourse.StudentId);
			cmd.AddParameter("@courseId", studentCourse.CourseId);
			return (int)con.ExecuteScalar(cmd);
		}
	}
}
