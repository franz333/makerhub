﻿ create table [Role] (
	[Id] int identity,
	[Type] varchar(50) not null,
	constraint PK_Role primary key ([Id]),
	constraint UK_Role_Name unique ([Type]),
 );