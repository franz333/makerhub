﻿create table Img (
	[Id] int identity,
	[Data] varbinary(max) not null default 0x11223344,
	[Name] nvarchar(250) not null default 'image Name',
	[Format] nvarchar(20) not null,
	Size int not null default 500, 
	Title nvarchar(150),
	Alt nvarchar(250) not null,
	Caption nvarchar(150),
	IsAdmin bit not null default 0,
	constraint PK_Img primary key ([Id]), 
);