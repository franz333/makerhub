﻿create table Badge (
	[Id] int Identity,
	Title nvarchar(100) not null,
	[Description] nvarchar(400) not null,
	Instruction nvarchar(400) not null,
	Active bit not null default 0,
	Deleted bit not null default 0,
	Img_Id int not null default 8,
	constraint PK_Badge primary key ([Id]),
	constraint UK_Badge_Title unique (Title),
	constraint FK_Badge_Img foreign key (Img_Id) references Img ([Id]) on delete set default on update cascade,
);