﻿create table Student (
	[Id] int identity,
	[User_Id] int not null,
	Img_Id int not null default 2,
	constraint PK_Student primary key ([Id]),
	constraint FK_Student_User foreign key ([User_Id]) references [User]([Id]),
	constraint FK_Student_Img foreign key (Img_Id) references Img ([Id]) on delete set default on update cascade,
	constraint UK_Student unique ([User_Id]),	
);