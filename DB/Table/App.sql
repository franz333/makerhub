﻿create table App (
	[Id] int default 1,
	Title nvarchar(50) not null default 'Europe & Un',
	Baseline nvarchar(150) not null default 'L''Europe, C''est simple, ça s''Apprend !',
	FB_URL nvarchar(100),
	Twitter_URL nvarchar(100),
	Credits nvarchar(200) not null default '© 2019 Europe&Un. Tous droits réservés.',
	Img_Id int not null
	constraint PK_App primary key ([Id]), 
	constraint UK_App_Title unique (Title),
);