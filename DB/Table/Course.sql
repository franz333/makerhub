﻿create table Course (
	[Id] int Identity,
	Title nvarchar(100) not null,
	[Description] nvarchar(1000) not null,
	Difficulty nvarchar(20) not null default 'facile', 
	Active bit not null default 0,
	Deleted bit not null default 0,
	IsSeen bit default 0,
	CreationDate datetime not null default getdate(),
	UpdateDate datetime default null,
	PublicationDate datetime default null, 
	User_Id_Creator int not null,
	User_Id_Publisher int,
	Certificate_Id int not null,
	Img_Id int not null default 4,
	constraint PK_Course primary key ([Id]),
	constraint UK_Course unique (Title, Difficulty),
	constraint FK_Course_User_Creator foreign key (User_Id_Creator) references [User] ([Id]),
	constraint FK_Course_User_Publisher foreign key (User_Id_Publisher) references [User] ([Id]),
	constraint FK_Course_Img foreign key (Img_Id) references Img ([Id]) on delete set default on update cascade,
	-- contrainte check la date de publication > date de création
	-- contrainte check la date de mise à jour > date de création
	-- trigger on update sur CreationDate insert value dans UpdateDate
	-- trigger si un cours est désactivé, les chapitres et lessons le seront aussi
	-- trigger si un cours est deleté pareil
);
