﻿create table PageImg (
	Page_Id int not null,
	Img_Id int not null,
	constraint PK_PageImg primary key (Page_Id, Img_Id), 
	constraint FK_PageImg_Img foreign key (Img_Id) references Img ([Id]),
	constraint FK_PageImg_Page foreign key (Page_Id) references [Page] ([Id]),
);
