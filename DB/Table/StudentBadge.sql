﻿create table StudentBadge (
	[Date] datetime2 not null default getdate(),
	Student_Id int not null,
	Badge_Id int not null,
	Active bit not null default 0,
	Deleted bit not null default 0,
	constraint PK_StudentBadge primary key (Student_Id, Badge_Id),
	constraint FK_StudentBadge_Student foreign key (Student_Id) references Student ([Id]),
	constraint FK_StudentBadge_Badge foreign key (Badge_Id) references Badge ([Id]),
);