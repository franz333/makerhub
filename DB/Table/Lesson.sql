﻿create table Lesson (
     [Id] int Identity,
	 Num int not null, 
     Title nvarchar(100) not null,
	 Active bit not null default 1,
	 Deleted bit not null default 0,
	 IsSeen bit default 0,
	 Chapter_Id int not null,
	 Img_Id int not null default 6,	
     constraint PK_Lesson primary key ([Id]),
	 constraint UK_Lesson unique (Title, Chapter_Id), 
     constraint FK_Lesson_Chapter foreign key (Chapter_Id) references Chapter ([Id]),
	 constraint FK_Lesson_Img foreign key (Img_Id) references Img ([Id]),
);
