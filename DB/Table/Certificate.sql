﻿create table [Certificate] (
	[Id] int Identity,
	Title nvarchar(100) not null,
	[Description] nvarchar(400) not null,
	Difficulty nvarchar(20) not null default 'facile',
	Active bit not null default 1,
	Deleted bit not null default 0,
	Img_Id int not null default 7,
	constraint PK_Certificate primary key ([Id]),
	constraint UK_Certificate_Title unique (Title),
	constraint FK_Certificate_Img foreign key (Img_Id) references Img ([Id]) on delete set default on update cascade, 
); 