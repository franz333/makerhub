﻿create table StudentCourse (
	BeginDate datetime2 not null default getdate(),
	Points int default 0,
	Student_Id int not null,
	Course_Id int not null,
	Deleted bit not null default 0,
	Bookmark nvarchar(12) not null default '00-00-00',
	constraint PK_StudentCourse primary key (Student_Id, Course_Id),
	constraint FK_StudentCourse_Student foreign key (Student_Id) references Student ([Id]),
	constraint FK_StudentCourse_Course foreign key (Course_Id) references Course ([Id]),
	constraint UK_StudentCourse unique (Student_Id, Course_Id),
);
