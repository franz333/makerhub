﻿create table QuizzField (
	[Id] int Identity,
	Content nvarchar(300) not null,
	Page_Id int not null,
	Active bit not null default 1,
	Deleted bit not null default 0,
	constraint PK_QuizzField primary key ([Id]),
	constraint FK_QuizzField_Page foreign key (Page_Id) references [Page] ([Id]),
);