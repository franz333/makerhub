﻿create table StudentCertificate (
	[Date] datetime2 not null default getdate(),
	Pdf varbinary(max) not null default 0x11223344,
	Student_Id int not null,
	Certificate_Id int not null,
	Active bit not null default 0,
	Deleted bit not null default 0,
	constraint PK_StudentCertificate primary key (Student_Id, Certificate_Id),
	constraint FK_StudentCertificate_Student foreign key (Student_Id) references Student ([Id]),
	constraint FK_StudentCertificate_Certificate foreign key (Certificate_Id) references [Certificate] ([Id]),
	constraint UK_StudentCertificate unique (Student_Id, Certificate_Id)
);
