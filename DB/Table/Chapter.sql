﻿create table Chapter (
	[Id] int Identity,
	Num int not null, 
	Title nvarchar(150) not null,
	Active bit not null default 0,
	IsSeen bit default 0,
	Deleted bit not null default 0,
	Course_Id int not null,
	Img_Id int not null default 5,
	constraint PK_Chapter primary key ([Id]),
	constraint UK_Chapter unique (Title, Num, Course_Id), 	-- vérifier si la contrainte liante vaut pour chaque item et leur unicité un cours sera jamais unique, mais ptet qu'un item pourra avoir deux fois le même number
	constraint FK_Chapter_Course foreign key (Course_Id) references Course ([Id]),
	constraint FK_Chapter_Img foreign key (Img_Id) references Img ([Id]),
);