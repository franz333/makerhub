﻿create table Moderator (
	[Id] int identity,
	[User_Id] int not null,
	Img_Id int not null default 1,
	Active bit not null default 1,
	Deleted bit not null default 0,
	constraint PK_Moderator primary key ([Id]),
	constraint FK_Moderator_User foreign key ([User_Id]) references [User]([Id]),	
	constraint FK_Moderator_Img foreign key (Img_Id) references Img ([Id]) on delete set default on update cascade,
	constraint UK_Moderator unique ([User_Id]),
);
