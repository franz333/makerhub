﻿
create table [User] (
	[Id] int identity,
	AccountDate datetime2 default getdate(),
	[Name] nvarchar(100) not null,
	Email nvarchar(80) not null, 
	Pwd nvarchar(30) not null,
	Active bit default 1, 
	Deleted bit default 0,
	Role_Type int not null,
	Token Uniqueidentifier,
	constraint PK_User primary key ([Id]),
	constraint UK_User_Email unique (Email),
	constraint FK_User_Role foreign key (Role_Type) references [Role] ([Id]),
	-- trigger pour limiter le nombre de user avec le rôle Moderator à 1 !
);
