﻿CREATE VIEW [dbo].[v_CoursesWithSignedInStudents]
	AS 
	SELECT 
	  DISTINCT C.[Id],
      C.[Title],
      C.[Description],
      C.[Difficulty],
      C.[Active],
      C.[Deleted],
      C.[IsSeen],
      C.[CreationDate],
      C.[UpdateDate],
      C.[PublicationDate],
      C.[User_Id_Creator],
      C.[User_Id_Publisher],
      C.[Certificate_Id],
      C.[Img_Id]
	FROM Course C
		join StudentCourse SC on SC.Course_Id = C.Id
	WHERE SC.Deleted = 0 and C.Active = 1;
GO