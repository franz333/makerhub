﻿CREATE VIEW [dbo].[v_StudentById]
	AS 
	SELECT 
		S.[Id],
		U.[Name],
		U.AccountDate,
		I.[Data]
	FROM [User] U  
			join Student S 
				on U.[Id] = S.[Id] 
			join [Img] I
				on S.[Id] = I.[Id]
	where U.Active = 1;
