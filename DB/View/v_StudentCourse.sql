﻿CREATE VIEW [dbo].[v_StudentCourse]
	AS 
	SELECT 
		SC.BeginDate,
		SC.Course_Id,
		SC.Points,
		SC.Student_Id,
		SC.Bookmark,
		C.[Id],
		C.Img_Id,
		C.[Title]
	FROM StudentCourse SC 
		join Course C 
			on SC.Course_Id = C.[Id] 
	where C.Active = 1;

