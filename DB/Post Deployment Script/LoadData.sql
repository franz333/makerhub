﻿/*
Modèle de script de post-déploiement							
--------------------------------------------------------------------------------------
 Ce fichier contient des instructions SQL qui seront ajoutées au script de compilation.		
 Utilisez la syntaxe SQLCMD pour inclure un fichier dans le script de post-déploiement.			
 Exemple :      :r .\monfichier.sql								
 Utilisez la syntaxe SQLCMD pour référencer une variable dans le script de post-déploiement.		
 Exemple :      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

use EUV40;

-- Images --
insert into Img ([Data], [Name], [Size], Title, Alt, Caption, [Format], IsAdmin) values (0x11223344, 'moderator-default-avatar',	500,  'Avatar par défaut du modérateur',	'Image par défaut représentant un modérateur du site',				'img caption',	'.png', 1);  
insert into Img ([Data], [Name], [Size], Title, Alt, Caption, [Format], IsAdmin) values (0x11223344, 'student-default-avatar',		500, 'Avatar par défault de l''étudiant',	'Image par défaut représentant l''étudiant',						'img caption',	'.png', 1);
insert into Img ([Data], [Name], [Size], Title, Alt, Caption, [Format], IsAdmin) values (0x11223344, 'association-default-avatar',	500, 'Avatar par défaut de l''association',	'Image par défaut représentant une association',					'img caption',	'.png', 1);
insert into Img ([Data], [Name], [Size], Title, Alt, Caption, [Format], IsAdmin) values (0x11223344, 'course-default-avatar',		500, 'Avatar par défaut d''un cours',		'Image par défaut représentant un cours',							'img caption',	'.svg', 1);
insert into Img ([Data], [Name], [Size], Title, Alt, Caption, [Format], IsAdmin) values (0x11223344, 'chapter-default-avatar',		500, 'Avatar par défault d''un chapitre',	'Image par défault représentant un chapitre du cours',				'img caption',	'.svg', 1);
insert into Img ([Data], [Name], [Size], Title, Alt, Caption, [Format], IsAdmin) values (0x11223344, 'lesson-default',				500, 'Avatar d''une leçon',					'Image représentant une leçon',										'img caption',	'.svg', 1);
insert into Img ([Data], [Name], [Size], Title, Alt, Caption, [Format], IsAdmin) values (0x11223344, 'certificat-ue',				500, 'Certificat cours Union européenne',	'Image représentant le certificat pour le cours Union européenne',	'img caption',	'.svg', 1);
insert into Img ([Data], [Name], [Size], Title, Alt, Caption, [Format], IsAdmin) values (0x11223344, 'badge-default',				500, 'Avatar par défault d''un badge ',		'Image représentant un badge ',										'img caption',	'.svg', 1);
insert into Img ([Data], [Name], [Size], Title, Alt, Caption, [Format], IsAdmin) values (0x11223344, 'logo-europe-et-un',			500, 'Logo d''Europe & Un',					'Image représentant le logo de Europe & Un',						'img caption',	'.svg', 1);

-- App --
insert into App (Title, Baseline, FB_URL, Twitter_URL, Credits, Img_Id) values ('Europe & Un', 'L''Europe c''est simple, ça sapprend', 'www.google.be', 'www.google.be', 'Credits Europe & Un 2019 - Tous droits réservés', 9);


-- Roles --
insert into [Role] ([Type]) values ('moderator');
insert into [Role] ([Type]) values ('student');
insert into [Role] ([Type]) values ('association');

-- Users --
insert into [User] (AccountDate, [Name], Email, Pwd, Active, Deleted, Role_Type) values (default, 'françois',			'f.duroy@gmail.com',	'test1234=',		default, default, 1); -- admin
insert into [User] (AccountDate, [Name], Email, Pwd, Active, Deleted, Role_Type) values (default, 'Jordan Poncelet',	'jponcelet@gmail.com',	'jordan1234',		default, default, 2); -- student
insert into [User] (AccountDate, [Name], Email, Pwd, Active, Deleted, Role_Type) values (default, 'MH',					'mh@gmail.com',			'mh1234',			default, default, 2); -- student
insert into [User] (AccountDate, [Name], Email, Pwd, Active, Deleted, Role_Type) values (default, 'Tristan',			'tristan@gmail.com',	'tristan1234',		default, default, 2); -- student
insert into [User] (AccountDate, [Name], Email, Pwd, Active, Deleted, Role_Type) values (default, 'Uniser',				'contact@uniser.it',	'uniser1234',		default, default, 3); -- association
insert into [User] (AccountDate, [Name], Email, Pwd, Active, Deleted, Role_Type) values (default, 'Compagnons',			'contact@cb.be',		'compagnons1234',	default, default, 3); -- association

-- Moderator --
insert into Moderator ([User_Id], Img_Id) values (1, default);

-- Student --
insert into Student ([User_Id], Img_Id) values (2, default);
insert into Student ([User_Id], Img_Id) values (3, default);
insert into Student ([User_Id], Img_Id) values (4, default);

-- Association --
insert into Asso ([User_Id], Street, Num, Box, City, PostCode, Country, Website, Email, [Description], PicNumber, Sector, Active, Deleted, Img_Id) values (5, 'Via Valverde', 15, null, 'Forlì', '5020', 'Italie', 'www.uniser.it', 'info@uniser.it', 'Lorem ipsum', 123456789, 'Mobilità per l''apprendimento', default, default, default);
insert into Asso ([User_Id], Street, Num, Box, City, PostCode, Country, Website, Email, [Description], PicNumber, Sector, Active, Deleted, Img_Id) values (6, 'Rue de la Gare', 8, null, 'Marche', '6950', 'Belgique', 'www.cb.be', 'sophie@cb.be', 'Lorem ipsum', 123456788, 'Association d''envoi projets européens', default, default, default);

-- Certificate -- 
insert into [Certificate] (Title, [Description], Difficulty, Active, Deleted, Img_Id) values ('L''UE',					'Lorem ipsum', default, default, default, default); 
insert into [Certificate] (Title, [Description], Difficulty, Active, Deleted, Img_Id) values ('Le Parlement européen',	'Lorem ipsum', default, default, default, default); 
insert into [Certificate] (Title, [Description], Difficulty, Active, Deleted, Img_Id) values ('Le Conseil Européen',	'Lorem ipsum', default, default, default, default); 
insert into [Certificate] (Title, [Description], Difficulty, Active, Deleted, Img_Id) values ('Erasmus+',				'Lorem ipsum', default, default, default, default); 

-- Cours --
insert into Course (Title, [Description], Difficulty, Active, Deleted, CreationDate, UpdateDate, PublicationDate, User_Id_Creator, User_Id_Publisher, Certificate_Id, Img_Id) values ('L''Union européenne',	'Lorem ipsum', default, 1, default, default, default, default, 1, 1, 1, 4); 
insert into Course (Title, [Description], Difficulty, Active, Deleted, CreationDate, UpdateDate, PublicationDate, User_Id_Creator, User_Id_Publisher, Certificate_Id, Img_Id) values ('Le Parlement européen',	'Lorem ipsum', default, 1, default, default, default, default, 5, 1, 2, 4); 
insert into Course (Title, [Description], Difficulty, Active, Deleted, CreationDate, UpdateDate, PublicationDate, User_Id_Creator, User_Id_Publisher, Certificate_Id, Img_Id) values ('Le Conseil européen',	'Lorem ipsum', default, 1, default, default, default, default, 1, 1, 3, 4); 
insert into Course (Title, [Description], Difficulty, Active, Deleted, CreationDate, UpdateDate, PublicationDate, User_Id_Creator, User_Id_Publisher, Certificate_Id, Img_Id) values ('Erasmus+',				'Lorem ipsum', default, 1, default, default, default, default, 6, 1, 4, 4); 

-- Badge --
insert into Badge (Title, [Description], Instruction, Active, Deleted, Img_Id) values ('Compte vérifié', 'L''utilisateur a vérifié son compte', 'Vérifie ton compte grâce au lien reçu par e-mail lors de l''inscription', default, default, 8);

-- StudentCourse -- 
insert into StudentCourse (BeginDate, Points, Student_Id, Course_Id, Deleted, Bookmark) values (default, default, 1, 1, default, default);
insert into StudentCourse (BeginDate, Points, Student_Id, Course_Id, Deleted, Bookmark) values (default, default, 1, 2, default, default);
insert into StudentCourse (BeginDate, Points, Student_Id, Course_Id, Deleted, Bookmark) values (default, default, 1, 4, default, default);
insert into StudentCourse (BeginDate, Points, Student_Id, Course_Id, Deleted, Bookmark) values (default, default, 3, 1, default, default);
insert into StudentCourse (BeginDate, Points, Student_Id, Course_Id, Deleted, Bookmark) values (default, default, 3, 2, default, default);

-- StudentCertificate -- 
insert into StudentCertificate ([Date], Pdf, Student_Id, Certificate_Id, Active, Deleted) values (default, default, 1, 1, default, default);
insert into StudentCertificate ([Date], Pdf, Student_Id, Certificate_Id, Active, Deleted) values (default, default, 3, 1, default, default);

-- StudentBadge -- 
insert into StudentBadge ([Date], Student_Id, Badge_Id, Active, Deleted) values (default, 1, 1, default, default); 
insert into StudentBadge ([Date], Student_Id, Badge_Id, Active, Deleted) values (default, 3, 1, default, default); 