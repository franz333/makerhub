﻿CREATE PROCEDURE sp_GetStudentsSignedInForCourse
  @courseId int
AS
SELECT 
	S.Id, S.User_Id, S.Img_Id
  
  FROM Student S
	join StudentCourse C on C.Student_Id = S.Id
  WHERE C.Course_Id = @courseId
  Order by C.Points desc
GO