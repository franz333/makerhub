
<!-- PROJECT LOGO -->
<br />
<div align="left" id="top">
  <a href="https://www.technobel.be/fr/">
    <img src="https://www.technobel.be/images/technobel_logo_hor.png" alt="Logo" width=300>
  </a>

  <h2 align="left">.Net Developer 9 months training</h2>
  <h3>Makerhub: Learn by doing</h3>
  <br />
  <br />
</div>


<!-- TABLE OF CONTENTS -->
  <ol>
    <li>
      <a href="#makerhub">Makerhub</a>
    </li>
    <li>
      <a href="#my-project-a-web-app-to-learn-about-europe">My Project: A Web App To Learn About Europe</a>
      <ul>
        <li><a href="#the-approach-the-full-lifecycle-of-a-product">The Approach: The Full Lifecycle Of A Product</a></li>
        <li><a href="#a-web-app-about-eu">A Web App About EU</a></li>
      </ul>
    </li>
    <li>
      <a href="#technologies">Technologies</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
        <li><a href="#nota-bene">Nota Bene</a></li>
      </ul>
    </li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
<br />

<!-- THE CONTEXT OF MAKERHUB -->
## Makerhub

Makerhub is the name of the project that each student has to realise during the 9 months training. 

It is a digital project like a web or mobile application that he/she has to code and present in front of a jury of experts in order to validate his training. 

The difficulty lies in the fact that students are attending courses 5 days per week so the remaining available time to be dedicated to the project is during evenings, weekends and bank holidays.
<br />
<br />

<!-- ABOUT THE PROJECT -->
## My Project: A Web App To Learn About Europe

### The Approach: The Full Lifecycle Of A Product

As we had to come up with a project, I thought I mighted as well try to implement what I thought was the 'full lifecycle of a product': Ideation, Design, Development, Production. 

<ol>
    <li>Ideation
        <ul>
            <li>Ideas research</li>
            <li>Marketing research</li>
            <li>Features requirements</li>
        </ul>
    </li>
    <li>Desgin
        <ul>
            <li>Sketch</li>
            <li>Workflow</li>
            <li>Wireframe the UI</li>
            <li>Pre-validation</li>
        </ul>
    </li>
    <li>Dev
        <ul>
            <li>Local Microsoft Sql Server + Database</li>
            <li>Backend: 1 Data Access Layer & 1 Business Access Layer</li>
            <li>Frontend: ASP.NET Razor Webpages + Jquery + Bootstrap
            <li>Asp.Net MVC software architectural pattern</li>
        </ul>
    </li>
    <li>Production
        <ul>
            <li>Local</li>
        </ul>
    </li>
</ol>

### A Web App About EU

After applying a "product design" approach, I came up with a project similar to [Sololearn](https://www.sololearn.com/?v=2) where users could read courses about the European Union, learn and answer quizzes:

<ul>
  <li>The Web App admninistrator has the ability to add content (courses and quizzes)</li>
  <li>The user can create an account and sign up to courses</li>
  <li>The App is fostering "competion" thanks to a Ranking board</li>
  <li>...</li>
</ul>

The result of my researches that led to this project are available in French via this: [PDF file](/Documentation/EuropeOne_v2.1.pdf)

The wireframe is still online and available on [Invision](https://projects.invisionapp.com/share/U8THA1N5YXK#/screens/378453818). It will give you an idea of the application I imagined which is - as you will probably notice - very much inspired by [Sololearn](https://www.sololearn.com/?v=2). 

<p align="center"><a href="#top">-- Back to top --</a></p>
<br />
<br />

<!-- TECHNOLOGIES -->
## Technologies

### Built With

This section list major tools/frameworks/libraries used to bootstrap my project. 

* [![Invision][Invision]][Invision-url]
* [![DBMain][DBMain]][DBMain-url]
* [![MicrosoftSQLServer][MicrosoftSQLServer]][MicrosoftSQLServer-url]
* [![VisualStudio][VisualStudio]][VisualStudio-url]
* [![CSharp][CSharp]][CSharp-url]
* [![Dotnet][DotNet]][DotNet-url]
* [![Bootstrap][Bootstrap.com]][Bootstrap-url]
* [![JQuery][JQuery.com]][JQuery-url]
* [![CKEditor][CKEditor]][CKEditor-url]
* [![BitBucket][BitBucket]][BitBucket-url]

### Nota Bene

During this 9 months training we studied a lot of different technologies: 

| Tech     | Days |
| ----------- | ----------- |
| Algorithm      | 12       |
| OOP with Python   | 12        |
| Databases: introduction   | 3        |
| HTML / CSS  | 5        |
| Javascript  | 5        |
| Git  | 1        |
| C#  | 19        |
| Scrum  | 3        |
| Databases: administration  | 3        |
| ASP (ADO.NET - LINQ - MVC)  | 13        |
| API Web with C#  | 1        |
| WPF  | 3        |
| MVVM  | 6        |
| Ajax  | 3        |
| Angular  | 5        |
| React  | 5        |
| Business Intelligence  | 4        |
| Signal R  | 3        |
| AI  | 5        |

This was certainly too many topics in such a "short period" of time. As a result we had enough theoretical knowledge to start using a few of them in our project. 
But Git requires some time before being comfortable and start to know what you are really doing. Most of us did not use it to version our code.

<p align="center"><a href="#top">-- Back to top --</a></p>
<br />
<br />

<!-- CONTACT -->
## Contact

[Linkedin](https://www.linkedin.com/in/francoisduroy/)

<br />
<br />

<!-- ACKNOWLEDGMENTS -->
## Acknowledgments

* [Img Shields](https://shields.io)
* [GitHub Pages](https://pages.github.com)
* [OthNeilDrew Readme Template](https://github.com/othneildrew/Best-README-Template)


<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=for-the-badge
[contributors-url]: https://github.com/othneildrew/Best-README-Template/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge
[forks-url]: https://github.com/othneildrew/Best-README-Template/network/members
[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=for-the-badge
[stars-url]: https://github.com/othneildrew/Best-README-Template/stargazers
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge
[issues-url]: https://github.com/othneildrew/Best-README-Template/issues
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/othneildrew
[product-screenshot]: images/screenshot.png

[Bootstrap.com]: https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white
[Bootstrap-url]: https://getbootstrap.com

[JQuery.com]: https://img.shields.io/badge/jQuery-0769AD?style=for-the-badge&logo=jquery&logoColor=white
[JQuery-url]: https://jquery.com 

[CSharp]: https://img.shields.io/badge/c%20sharp-239120?style=for-the-badge&logo=csharp&logoColor=white
[CSharp-url]: https://learn.microsoft.com/en-us/dotnet/csharp/

[DotNet]: https://img.shields.io/badge/.NET-512BD4?style=for-the-badge&logo=dotnet&logoColor=white
[DotNet-url]: https://dotnet.microsoft.com/

[MicrosoftSQLServer]: https://img.shields.io/badge/microsoft%20sql%20server-CC2927?style=for-the-badge&logo=microsoftsqlserver&logoColor=white
[MicrosoftSQLServer-url]: https://www.microsoft.com/en-us/sql-server/

[Invision]:https://img.shields.io/badge/invision-FF3366?style=for-the-badge&logo=invision&logoColor=white
[Invision-url]: https://www.invisionapp.com/

[DBMain]: https://img.shields.io/badge/DB%20Main-DB%20Main-red
[DBMain-url]: https://www.db-main.eu/

[VisualStudio]: https://img.shields.io/badge/Visual%20Studio-5C2D91?style=for-the-badge&logo=visualstudio&logoColor=white
[VisualStudio-url]: https://visualstudio.microsoft.com/

[BitBucket]: https://img.shields.io/badge/Bitbucket-0052CC?style=for-the-badge&logo=bitbucket&logoColor=white
[BitBucket-url]: https://bitbucket.org/


[CKEditor]: https://img.shields.io/badge/CKEditor-0287D0?style=for-the-badge&logo=ckeditor4&logoColor=white
[CKEditor-url]: https://ckeditor.com/