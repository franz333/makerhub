﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DBConnection
{
	public class Connexion
	{

		// ********* FIELDS ********* //

		#region Fields

		private DbProviderFactory Factory; // on ne peut pas l'initialiser car on ne sait pas quel type de BD on aura...on le fera dans le constructeur, quand on initialisera notre connexion
		private string _cString;

		#endregion

		// ********* PROPERTIES ********* //

		#region Properties

		private string CString
		{
			get => _cString; // pour le return, la lambda est plus facile (la lambda est devenue la norme)

			// Dans un setter on vérifie toujours si tout va mal, pas si tout va bien (test de garde)
			set
			{
				DbConnection connexion = Factory.CreateConnection();
				connexion.ConnectionString = value;
				connexion.Open();
				if (connexion.State != ConnectionState.Open)
				{
					throw new Exception("Connection string invalide");
				}
				_cString = value;
				connexion.Dispose();
			}
		}

		#endregion

		// ********* CONSTRUCTOR ********* //

		#region Constructor

		public Connexion(string connectionString, string Invariantname = "System.Data.SqlClient")
		{
			if (string.IsNullOrWhiteSpace(Invariantname))
			{
				throw new Exception("Invariant name not valid");
			}
			this.Factory = DbProviderFactories.GetFactory(Invariantname); // on définit la factory qui nous concerne dans le constructeur, le type de DB sera fourni par Invariantname
			this.CString = connectionString;
		}

		#endregion


		// ********* METHODS ********* //

		#region Methods

		// ExecuteScalar
		public object ExecuteScalar(Command cmd)
		{
			using (DbConnection connexion = this.CreateConnection())
			{
				using (DbCommand command = this.CreateCommand(connexion, cmd))
				{
					return command.ExecuteScalar();
				}
			}
		}

		// ExecuteReader
		public IEnumerable<T> ExecuteReader<T>(Command cmd, Func<IDataReader, T> convert) where T : new() // where nous permet de dire que T est objet de classe
		{
			// le delegate func nous permet de convertir ce qu'on va avoir en db
			using (DbConnection connexion = this.CreateConnection())
			{
				using (DbCommand command = this.CreateCommand(connexion, cmd))
				{
					using (DbDataReader reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							yield return convert(reader); // A utiliser quand on parcourt une liste, le yield nous permet de parcourir un ienumerable sans faire de collection list ou autre
						}
					}
				}
			}
		}

		// ExecuteNonQuery
		public int ExecuteNonQuery(Command cmd)
		{
			using (DbConnection connexion = this.CreateConnection())
			{
				using (DbCommand command = this.CreateCommand(connexion, cmd))
				{
					return command.ExecuteNonQuery();
				}
			}
		}

		// GetDataTable
		public DataTable GetDataTable(Command cmd)
		{
			using (DbConnection connexion = this.CreateConnection())
			{
				using (DbCommand command = this.CreateCommand(connexion, cmd))
				{
					DataTable result = new DataTable();
					DbDataAdapter adapter = Factory.CreateDataAdapter();
					adapter.SelectCommand = command;
					adapter.Fill(result);
					return result;
				}
			}
		}

		// GetDataSet
		public DataSet GetDataSet(Command cmd)
		{
			using (DbConnection connexion = this.CreateConnection())
			{
				using (DbCommand command = this.CreateCommand(connexion, cmd))
				{
					DataSet result = new DataSet();
					DbDataAdapter adapter = Factory.CreateDataAdapter();
					adapter.SelectCommand = command;
					adapter.Fill(result);
					return result;
				}
			}
		}

		// CreateConnection
		private DbConnection CreateConnection()
		{
			DbConnection connexion = Factory.CreateConnection();
			connexion.ConnectionString = this.CString;
			connexion.Open();
			return connexion;
		}

		// CreateCommand
		private DbCommand CreateCommand(DbConnection connexion, Command cmd)
		{
			DbCommand command = connexion.CreateCommand();
			command.CommandText = cmd.Query;
			command.CommandType = (cmd.IsStoredProcedure) ? CommandType.StoredProcedure : CommandType.Text;

			// Modification en prenant en compte le DBType paramater
			foreach (KeyValuePair<string, Parameter> kvp in cmd.Parameters)
			{
				DbParameter param = Factory.CreateParameter();
				param.ParameterName = kvp.Key;
				param.Value = kvp.Value.Value;
				if (kvp.Value.Type != null)
				{
					param.DbType = (DbType)kvp.Value.Type;
				}
				command.Parameters.Add(param);
			}
			return command;
		}

		#endregion
	}
}
