﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DBConnection
{
	public class Command
	{

		// ********* FIELDS ********* //

		#region Fields

		private string _query;

		#endregion

		// ********* PROPERTIES ********* //

		#region Properties

		public string Query
		{
			get => _query;
			set
			{
				if (string.IsNullOrWhiteSpace(value))
				{
					throw new Exception("Requête invalide");
				}
				_query = value;
			}
		}

		#endregion

		// ********* PARAMETERS ********* //

		#region Parameters

		private Dictionary<string, Parameter> _parameters;

		public Dictionary<string, Parameter> Parameters
		{
			get { return _parameters; }
			set { _parameters = value; }
		}

		// Stored Procedure : pour spécifier si notre requête est une procédure stockée ou pas
		private bool _isStoredProcedure;

		public bool IsStoredProcedure
		{
			get { return _isStoredProcedure; }
			set { _isStoredProcedure = value; }
		}

		#endregion

		// ********* CONSTRUCTOR ********* //

		#region Constructors

		public Command(string query, bool isStoredProcedure = true)
		{
			this.Query = query;
			this.IsStoredProcedure = isStoredProcedure;
			this.Parameters = new Dictionary<string, Parameter>();
		}

		public Command(string query, Dictionary<string, Parameter> parameters, bool isStoredProcedure = true) : this(query, isStoredProcedure) // le deuxième constructeur rappelle le premier par héritage
		{
			if (parameters != null)
			{
				foreach (KeyValuePair<string, Parameter> kvp in parameters)
				{
					this.AddParameter(kvp.Key, kvp.Value.Value);
				}
			}
		}

		#endregion

		// ********* METHODS ********* //

		#region Methods
		/// <summary>
		///  Ajoute un paramètre à la propriété Parameters de la class Commande si celui-ci est valie
		/// </summary>
		/// <param name="name">Nom du paramètre</param>
		/// <param name="value">Valeur du paramètre</param>
		/// 

		public void AddParameter(string name, object value)
		{
			if (!string.IsNullOrWhiteSpace(name))
			{
				this.Parameters.Add(name, new Parameter { Value = value ?? DBNull.Value });
			}
		}

		// Méthodes complémentaire à ce qu'on avait avec SAM pour gérer l'insertion d'image..
		// Il faut absolument spécifier au dictionnaire de parameter qu'on aura un paramètre DBType lorsqu'on travaillera avec des images
		public void AddParameter(string name, object value, DbType type)
		{
			if (!string.IsNullOrWhiteSpace(name))
			{
				this.Parameters.Add(name, new Parameter { Value = value ?? DBNull.Value, Type = type });
			}
		}
		#endregion
	}
}
